import os
import sys
import requests
import time
import yaml
import secrets
from datetime import datetime
from requests.exceptions import Timeout

try:
    assert os.environ['SPT_API_TOKEN']
except:
    print(f'''Export an environment variable SPT_API_TOKEN containing your API token prior to executing this script:
    export SPT_API_TOKEN="eyJ0eXAiO...f72LVU"
    python {sys.argv[0]}
    ''')
    sys.exit(1)


CONFIG = {
    'auth_token': os.environ['SPT_API_TOKEN'],
    'apiBaseUrl': 'https://spt3g.ncsa.illinois.edu/api/v1',
    'filesBaseUrl': 'https://spt3g.ncsa.illinois.edu/files/jobs',
}


def submit_cutout_job(
    positions: str = '',
    date_start: str = '',
    date_end: str = '',
    bands: list = None,
    filetypes: list = None,
    yearly_coadd: list = None,
    email: bool = False,
    environment: list = [],
) -> dict:
    job_info = {}
    """Submits a cutout job and returns the complete server response which includes the job ID."""
    ## Validate inputs
    assert positions and date_start and date_end
    data = {
        'positions': positions,
        'date_start': date_start,
        'date_end': date_end,
        'email': email,
    }
    if bands:
        data['bands'] = bands
    if filetypes:
        data['filetypes'] = filetypes
    if yearly_coadd:
        data['yearly_coadd'] = yearly_coadd
    if environment:
        data['environment'] = environment
    ## Submit job
    response = requests.request('PUT',
        f'''{CONFIG['apiBaseUrl']}/uws/job''',
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
        json=data,
    )
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except:
        print(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def get_job_status(job_id: str = '') -> list:
    """Get status of individual job or all jobs belonging to the authenticated user."""
    job_info = {}
    ## Validate inputs
    assert isinstance(job_id, str) or not job_id
    url = f'''{CONFIG['apiBaseUrl']}/uws/job'''
    if job_id:
        url += f'''/{job_id}'''
    ## Fetch job status
    try:
        response = requests.request('GET',
            url,
            headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
            timeout=3,
        )
    except Timeout:
        print(f'''ERROR: Timeout fetching job status''')
        return job_info
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except:
        print(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def delete_job(job_id: str = '') -> list:
    """Delete individual job"""
    ## Validate inputs
    assert isinstance(job_id, str) and job_id
    ## Delete job
    try:
        response = requests.request('DELETE',
            f'''{CONFIG['apiBaseUrl']}/uws/job/{job_id}''',
            headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
        )
    except Timeout:
        print(f'''ERROR: Timeout requesting job deletion''')
        return False
    try:
        assert response.status_code in [200, 204]
        return True
    except:
        print(f'''[{response.status_code}] {response.text}''')
        return False


def job_status_poll(job_id):
    print(f'Polling status of job "{job_id}"...', end='')
    job_info = {}
    while not job_info or ('phase' in job_info and job_info['phase'] in ['pending', 'queued', 'executing']):
        print('.', end='', sep='', flush=True)
        # Fetch the current job status
        job_info = get_job_status(job_id)
        # print(json.dumps(job_info))
        time.sleep(3)
    print('\n')
    return job_info


def download_job_files(job_id=None, base_url=None, download_dir=None):
    if not job_id:
        return
    if not base_url:
        base_url = f'''{CONFIG["filesBaseUrl"]}/{job_id}/out'''
    if not download_dir:
        download_base_dir = os.path.abspath(os.getcwd())
        download_sub_dir = os.path.join('cutout_job_files', job_id)
        download_dir = os.path.join(download_base_dir, download_sub_dir)
        print(f'''Downloading job files to "{download_sub_dir}"...''')
    os.makedirs(download_dir, exist_ok=True)
    response = requests.request('GET', f'''{base_url}/json''')
    for item in response.json():
        suburl = f'''{base_url}/{item['name']}'''
        if item['type'] == 'directory':
            subdir = f'''{download_dir}/{item['name']}'''
            download_job_files(job_id=job_id, base_url=suburl, download_dir=subdir)
        elif item['type'] == 'file':
            try:
                data = requests.request('GET', suburl, stream=True)
                with open(f'''{download_dir}/{item['name']}''', "wb") as file:
                    for chunk in data.iter_content(chunk_size=8192):
                        file.write(chunk)
            except Exception as e:
                print(f'''Error fetching result file "{suburl}": {e}''')

def download_job_log(job_id):
    url = f'''{CONFIG["filesBaseUrl"]}/{job_id}/out/job.log'''
    try:
        data = requests.request('GET', url, stream=True)
        with open(f'''{job_id}.log''', "wb") as file:
            for chunk in data.iter_content(chunk_size=8192):
                file.write(chunk)
    except Exception as e:
        print(f'''Error fetching log file "{url}": {e}''')

def main():
    
    import argparse

    parser = argparse.ArgumentParser(description='Launch jobs to test job system.')
    parser.add_argument('--num', nargs='?', type=int, default=0, help='number of jobs to launch')
    parser.add_argument('--poll', action='store_true')
    parser.add_argument('--delete', action='store_true')
    args = parser.parse_args()
    
    ## Delete all jobs
    ##
    if args.delete:
        jobs = get_job_status()
        for job_info in jobs:
            print(f'''Deleting {job_info['job_id']}...''')
            delete_job(job_info['job_id'])
        sys.exit(0)

    ## Submit new jobs
    ##
    job_ids = []
    job_idx = 0
    while job_idx < args.num:
        ## Select a random job config
        fileIdx = secrets.choice(range(1, 6))
        # fileIdx = 1
        fileName = f'''job_config.{fileIdx}.yaml'''
        # fileName = f'''job_config.yaml'''
        
        ## Load job configuration
        ##
        with open(fileName, "r") as conf_file:
            job_config = yaml.load(conf_file, Loader=yaml.FullLoader)
        # print(yaml.dump(job_config, indent=2))
        
        ## Submit new cutout job
        ##
        print(f'Submitting new cutout job ({fileName})...')
        job_info = submit_cutout_job(
            positions=job_config['positions'],
            date_start=job_config['date_start'],
            date_end=job_config['date_end'],
            yearly_coadd=job_config['yearly_coadd'] if 'yearly_coadd' in job_config else None,
            bands=job_config['bands'] if 'bands' in job_config else None,
            email=job_config['email'] if 'email' in job_config else False,
            filetypes=job_config['filetypes'] if 'filetypes' in job_config else None,
            environment=[{'name': 'LOG_LEVEL', 'value': 'DEBUG'}]
        )
        if not job_info:
            return
        job_id = job_info['jobId']
        print(f'Job "{job_id}" created.')
        job_ids.append(job_id)
        job_idx += 1
    
    ## Poll status of jobs
    ##
    if args.poll:
        num_jobs = -1
        num_incomplete = -1
        while num_jobs < 0 or num_incomplete != 0:
            num_complete = 0
            num_incomplete = 0
            ## Poll the status of the jobs
            ##
            print(f'''[{datetime.now().strftime('%H:%M:%S')}] Polling job status...''')
            for job_id in job_ids:
                job_status = get_job_status(job_id=job_id)
                if not job_status:
                    print(f'''[{datetime.now().strftime('%H:%M:%S')}] ERROR fetching job status.''')
                    num_incomplete = -1
                    break
                else:
                    num_jobs = len(job_ids)
                    job_info = job_status
                    phase = job_info['phase']
                    if phase in ['completed', 'aborted', 'error']:
                        num_complete += 1
                        download_job_log(job_info['job_id'])
                    else:
                        num_incomplete += 1
            print(f'''[{datetime.now().strftime('%H:%M:%S')}] {num_complete}/{num_jobs} jobs complete.''')
            time.sleep(5)

    # for job_id in job_ids:
    #     url = f'''{CONFIG["filesBaseUrl"]}/{job_id}/out/job.log'''
    #     try:
    #         data = requests.request('GET', url, stream=True)
    #         with open(f'''{job_id}.log''', "wb") as file:
    #             for chunk in data.iter_content(chunk_size=8192):
    #                 file.write(chunk)
    #     except Exception as e:
    #         print(f'''Error fetching log file "{url}": {e}''')

if __name__ == "__main__":
    main()
