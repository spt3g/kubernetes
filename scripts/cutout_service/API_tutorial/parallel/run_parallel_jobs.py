#!/usr/bin/env python3

import os
import sys
import requests
import time
import yaml
import csv
from concurrent.futures import ThreadPoolExecutor

'''
This script will split the positions list into chunks of size $SPT_CHUNK_SIZE
and run the resulting set of jobs in parallel (limited only by the available
resources on the cluster). If jobs fail, they will be retried up to the number
of times specified by $SPT_JOB_RETRIES. When all jobs are finished, the output
for each will be downloaded.
'''

try:
    assert os.environ['SPT_API_TOKEN']
except KeyError:
    print(f'''Export an environment variable SPT_API_TOKEN containing your API token prior to executing this script:
    export SPT_API_TOKEN="eyJ0eXAiO...f72LVU"
    python {sys.argv[0]}
    ''')
    sys.exit(1)


CONFIG = {
    'auth_token': os.environ['SPT_API_TOKEN'],
    'apiBaseUrl': 'https://spt3g.ncsa.illinois.edu/api/v1',
    'filesBaseUrl': 'https://spt3g.ncsa.illinois.edu/files/jobs',
    'chunk_size': int(os.environ.get("SPT_CHUNK_SIZE", "100")),
    'job_retries': int(os.environ.get("SPT_JOB_RETRIES", "1")),
}


def submit_cutout_job(**kw):

    data = kw
    # Submit job
    response = requests.request('PUT',
                                f'''{CONFIG['apiBaseUrl']}/uws/job''',
                                headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
                                json=data,)
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except AssertionError:
        print(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def get_job_status(job_id: str = '') -> list:
    """
    Get status of individual job or all jobs belonging to the authenticated
    user.
    """
    job_info = {}
    # Validate inputs
    assert isinstance(job_id, str) or not job_id
    url = f'''{CONFIG['apiBaseUrl']}/uws/job'''
    if job_id:
        url += f'''/{job_id}'''
    # Fetch job status
    response = requests.request('GET',
                                url,
                                headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
                                )
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except AssertionError:
        print(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def delete_job(job_id: str = '') -> list:
    """Delete individual job"""
    # Validate inputs
    assert isinstance(job_id, str) and job_id
    # Delete job
    response = requests.request('DELETE',
                                f'''{CONFIG['apiBaseUrl']}/uws/job/{job_id}''',
                                headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
                                )
    try:
        assert response.status_code in [200, 204]
        return True
    except AssertionError:
        print(f'''[{response.status_code}] {response.text}''')
        return False


def job_status_poll(job_id):
    print(f'Polling status of job "{job_id}"...')
    # print(f'Polling status of job "{job_id}"...', end='')
    job_info = {}
    while not job_info or ('phase' in job_info and job_info['phase'] in ['pending', 'queued', 'executing']):
        # print('.', end='', sep='', flush=True)
        # Fetch the current job status
        job_info = get_job_status(job_id)
        # print(json.dumps(job_info))
        time.sleep(3)
    # print('\n')
    return job_info


def download_job_files(job_id=None, base_url=None, download_dir=None):
    if not job_id:
        return
    if not base_url:
        base_url = f'''{CONFIG["filesBaseUrl"]}/{job_id}/out'''
    if not download_dir:
        download_base_dir = os.path.abspath(os.getcwd())
        download_sub_dir = os.path.join('cutout_job_files', job_id)
        download_dir = os.path.join(download_base_dir, download_sub_dir)
        print(f'''Downloading job files to "{download_sub_dir}"...''')
    os.makedirs(download_dir, exist_ok=True)
    response = requests.request('GET', f'''{base_url}/json''')
    for item in response.json():
        suburl = f'''{base_url}/{item['name']}'''
        if item['type'] == 'directory':
            subdir = f'''{download_dir}/{item['name']}'''
            download_job_files(job_id=job_id, base_url=suburl, download_dir=subdir)
        elif item['type'] == 'file':
            try:
                data = requests.request('GET', suburl, stream=True)
                with open(f'''{download_dir}/{item['name']}''', "wb") as file:
                    for chunk in data.iter_content(chunk_size=8192):
                        file.write(chunk)
            except Exception as e:
                print(f'''Error fetching result file "suburl": {e}''')

def monitor_job(job_id):
    # job_id = job_info['jobId']

    # Poll the status of the job
    job_status = job_status_poll(job_id)
    if not job_status:
        return False
    phase = job_status['phase']
    
    if phase != 'completed':
        print(f'''Error: Job "{job_id}" did not complete successfully. Status: "{phase}"''')

    # # Download the job output files
    # time.sleep(10)
    # download_job_files(job_id=job_id)
    # print('Done.')
    return job_status

def get_job_by_id(jobs, job_id):
    for idx, job in enumerate(jobs):
        if job['job_id'] == job_id:
            return idx, job
    return None

def update_job_info(jobs=[], job_id='', new_job_id='', phase=None, retries=None):
    if not jobs:
        return jobs
    # job_idx = [idx for idx in jobs if jobs[idx]['job_id'] == job_id][0]
    job_idx, _ = get_job_by_id(jobs, job_id=job_id)
    if new_job_id:
        jobs[job_idx]['job_id'] = new_job_id
    if phase:
        jobs[job_idx]['phase'] = phase
    if retries is not None:
        # print(f'''original retries: {jobs[job_idx]['retries']}''')
        jobs[job_idx]['retries'] = retries
        # print(f'''new retries: {jobs[job_idx]['retries']}''')
    return jobs

def monitor_jobs(jobs):
    coroutines = []
    with ThreadPoolExecutor() as executor:
        for job in jobs:
            coroutines.append(executor.submit(monitor_job, job['job_id']))
    
    results = {}
    while True:
        if len(results) == len(coroutines):
            break
        for coroutine in coroutines:
            try:
                job_info = coroutine.result(timeout=5)
                job_id = job_info['job_id']
                if job_info['phase'] == 'completed':
                    retries = 0
                else:
                    _, job = get_job_by_id(jobs, job_id=job_id)
                    retries = job['retries']
                jobs = update_job_info(jobs, job_id=job_id, phase=job_info['phase'], retries=retries)
                results[job_id] = {
                    'job_id': job_info['job_id'],
                    'phase': job_info['phase'],
                }
                print(f'Results for job {job_id} received.')
            except:
                continue
    return jobs

def main():

    # Load job configuration
    with open('job_config.yaml', "r") as conf_file:
        job_config = yaml.load(conf_file, Loader=yaml.FullLoader)
    # print(yaml.dump(job_config, indent=2))

    positions_text = job_config['positions']
    ## Parse positions as CSV data
    positions = []
    from io import StringIO
    with StringIO(positions_text) as positions_file:
        positions_file.flush
        reader = csv.DictReader(positions_file)
        positions_headers = reader.fieldnames
        for row in reader:
            # print(row)
            positions.append(row)
    chunk_size = CONFIG['chunk_size']
    chunks = []
    while positions:
        chunk, positions = positions[:chunk_size], positions[chunk_size:]
        chunks.append(chunk)
        # print(f'''\nChunk #{len(chunks)} ({len(chunk)})''')
        # print(f'''\nChunk #{len(chunks)} ({len(chunk)}):\n{yaml.dump(chunk, indent=2)}''')
    jobs = []
    for chunk in chunks:
        job_config_chunked = dict(job_config)
        job_config_chunked['positions'] = f'''{','.join(positions_headers)}\n'''
        for position in chunk:
            columns = []
            for header in positions_headers:
                columns.append(position[header])
            job_config_chunked['positions'] += f'''{','.join(columns)}\n'''
        print(f'''Submitting new cutout job #{len(jobs)}...''')
        job_info = submit_cutout_job(**job_config_chunked)
        # print(yaml.dump(job_info))
        jobs.append({
            'job_id': job_info['jobId'],
            'phase': job_info['phase'],
            'retries': CONFIG['job_retries'],
            'config': job_config_chunked,
            # 'job_info': job_info,
        })

    if not jobs:
        return

    jobs = monitor_jobs(jobs)

    retrying = True
    while retrying:
        retrying = False
        ## If job retries are allowed, run failed jobs again
        old_jobs = jobs.copy()
        for job_idx, job in enumerate(old_jobs):
            # print(yaml.dump(job))
            retries = job['retries']
            job_id = job['job_id']
            if job['phase'] != 'completed':
                if retries > 0:
                    retrying = True
                    print(f'''Retrying cutout job #{job_idx} ({job_id})...''')
                    job_info = submit_cutout_job(**job['config'])
                    new_job_id = job_info['jobId']
                    print(f'''Cutout job #{job_idx} ID {job_id} -> {new_job_id}''')
                    new_retries = retries-1
                    # print(f'''Cutout job #{job_idx} new retries should be {new_retries}''')
                    jobs = update_job_info(jobs=jobs, job_id=job_id,
                        new_job_id=new_job_id, 
                        phase=job_info['phase'],
                        retries=new_retries,
                    )
                    _, new_job = get_job_by_id(jobs=jobs, job_id=new_job_id)
                    print(f'''Retries remaining: {new_job['retries']}.''')
                else:
                    print(f'''Job #{job_idx} ({job_id}) retry limit reached. Failed.''')
        
        # print(yaml.dump(jobs))
        if retrying:
            print(f'''Monitoring retried jobs...''')
            jobs = monitor_jobs(jobs)

    # print(yaml.dump(jobs))

    for job in jobs:
        job_id = job['job_id']
        print(f'''Downloading job {job_id} output...''')
        download_job_files(job_id=job_id)

    print('Done.')
        
if __name__ == "__main__":
    main()
