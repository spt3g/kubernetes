import requests
import json
import os
import sys

'''
Test client for UWS API server
==============================

Set environment variable SPT3G_API_TOKEN with your API access token.

    export SPT3G_API_TOKEN='token_value'
    python client.py

Use kubectl port-forwarding to develop the API if the service is not accessible by ingress

    $ kubectl port-forward -n spt-api-dev svc/spt-api-dev 8080:80
    Forwarding from 127.0.0.1:8080 -> 8080
    Forwarding from [::1]:8080 -> 8080
    ...

'''

## Configure the options
protocol = 'https'
domain = 'spt3g.ncsa.illinois.edu'
port = '443'
basePath = 'api/v1/uws'

## Package the configuration
auth = os.environ['SPT3G_API_TOKEN']
auth_header = {'Authorization': f'Bearer {auth}'}
apiBaseUrl = f'''{protocol}://{domain}:{port}/{basePath.strip('/')}'''
config = dict(
    basePath=basePath,
    apiBaseUrl=apiBaseUrl,
    auth_header=auth_header,
)


def get_result(job_id=None, result=None):
    url = f'{config["apiBaseUrl"]}/job/result/{job_id}/{result["id"]}'
    try:
        local_filepath = os.path.join(os.path.abspath(os.getcwd()), result['uri'].strip('/'))
        print(f'''Downloading result {result["id"]} to "{result['uri'].strip('/')}"...''')
        with requests.get(
            url,
            stream=True,
            headers=config['auth_header'],
        ) as r:
            r.raise_for_status()
            os.makedirs(os.path.dirname(local_filepath), exist_ok=True)
            with open(local_filepath, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
        print(f'    Download complete.')
        return local_filepath
    except Exception as e:
        print(f'Error fetching result file: {str(e)}')
        return None


def list_jobs(phase=''):
    phaseQuery = f'?phase={phase}' if phase else ''
    url = f'{config["apiBaseUrl"]}/job{phaseQuery}'
    response = requests.get(
        url,
        headers=config['auth_header'],
    )
    try:
        responseText = json.dumps(response.json(), indent=2)
    except:
        responseText = json.dumps(response.text)
    print(f'GET {url} :\nHTTP code: {response.status_code}\n{responseText}\n\n')
    return response


def get_job(job_id, property=None):
    if property:
        response = requests.get(
            '{}/job/{}/{}'.format(config['apiBaseUrl'], job_id, property),
            headers=config['auth_header'],
        )
        try:
            print('GET {}/job/{}/{} :\nHTTP code: {}\n{}\n\n'.format(
                config['basePath'], job_id, property, response.status_code, json.dumps(response.json(), indent=2)))
        except:
            print('GET {}/job/{}/{} :\nHTTP code: {}\n{}\n\n'.format(
                config['basePath'], job_id, property, response.status_code, response))
    else:
        response = requests.get(
            '{}/job/{}'.format(config['apiBaseUrl'], job_id),
            headers=config['auth_header'],
        )
        try:
            print('GET {}/job/{} :\nHTTP code: {}\n{}\n\n'.format(
                config['basePath'], job_id, response.status_code, json.dumps(response.json(), indent=2)))
        except:
            print('GET {}/job/{} :\nHTTP code: {}\n{}\n\n'.format(
                config['basePath'], job_id, response.status_code, response))
    return response


def create_job(command='sleep 120', run_id=None, environment=[], git_url=None, commit_ref=None):
    payload = {
        'command': command,
        'run_id': run_id,
        'environment': environment,
        'url': git_url,
        'commit_ref': commit_ref,
    }
    url = f'{config["apiBaseUrl"]}/job'
    response = requests.put(
        url=url,
        json=payload,
        headers=config['auth_header'],
    )
    try:
        responseText = json.dumps(response.json(), indent=2)
    except:
        responseText = response.text
    print(f'PUT {url} :\nHTTP code: {response.status_code}\n{responseText}\n\n')
    return response


def delete_job(job_id):
    response = requests.delete(
        '{}/job/{}'.format(config['apiBaseUrl'], job_id),
        headers=config['auth_header'],
    )
    # print(response)
    try:
        print('DELETE {}/job/{} :\nHTTP code: {}\n{}\n\n'.format(
            config['basePath'], job_id, response.status_code, json.dumps(response.json(), indent=2)))
        return response.json()
    except:
        print('DELETE {}/job/{} :\nHTTP code: {}\n{}\n\n'.format(
            config['basePath'], job_id, response.status_code, response.text))
        return response.text


if __name__ == '__main__':
    import time

    # results = get_job('54f1a27a9a7d4d7596cbf9f26910f1ce', property='results').json()
    # print(results)
    # sys.exit()
    
    print('Create a job:')
    payload_env = dict(
        CUSTOM_ENV_VAR="Hello SPT3G job system!",
    )
    create_response = create_job(
        run_id='hello-world',
        command='cd $JOB_SOURCE_DIR && echo $CUSTOM_ENV_VAR && ls -lan /data/spt3g 2>&1 | tee -a $JOB_OUTPUT_DIR/job.log',
        environment=[dict(name=k, value=v) for k, v in payload_env.items()],
        # git_url='https://github.com/lsst-dm/uws-api-server',
        # commit_ref='d044eee155f1019c4da737271653a21d9907601c',
    )

    if create_response.status_code != 200:
        print("ERROR. Aborting.")
        sys.exit(1)
    else:
        job_id = create_response.json()['jobId']

    print('List jobs that are executing:')
    list_jobs(phase='executing')

    print('Get the phase of the job just created:')
    job_phase = get_job(job_id, property='phase').json()
    while job_phase in ['pending', 'queued', 'executing']:
        print(f'Job {job_id} phase is {job_phase}. Waiting to complete...')
        time.sleep(3)
        job_phase = get_job(job_id, property='phase').json()
    print(f'Job phase is {job_phase}.')

    # Show output files
    if job_phase == 'completed':
        wait_sec = 0
        print(f'Fetching results (wait {wait_sec} seconds)...')
        time.sleep(wait_sec)
        results = get_job(job_id, property='results').json()
        for result in results:
            downloaded_file = get_result(job_id=job_id, result=result)
            # if downloaded_file:
            #     print(f'Contents of result file "{downloaded_file}":')
            #     with open(downloaded_file, 'r') as dfile:
            #         print(dfile.read())

    ## DELETE ALL JOBS AND JOB FILES:
    # for job in list_jobs().json():
    #     # if job['runId'] == '12345678':
    #         print(f'Deleting job {job["jobId"]}...')
    #         delete_job(job['jobId'])
