CREATE TABLE IF NOT EXISTS `register`(
	`id` int NOT NULL AUTO_INCREMENT,
	`user_id` varchar(50) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL DEFAULT '',
  `time_registered` datetime NOT NULL DEFAULT 0,
  `email_sent` boolean NOT NULL DEFAULT 0,
  `approved` boolean NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`), UNIQUE KEY `user_id` (`user_id`)
)
