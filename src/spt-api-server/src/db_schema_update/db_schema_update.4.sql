CREATE TABLE IF NOT EXISTS `job`(
	`id` int NOT NULL AUTO_INCREMENT,
	`job_id` varchar(32) NOT NULL,
	`run_id` varchar(64) NOT NULL,
	`user_id` varchar(50) NOT NULL,
  `command` text NOT NULL DEFAULT '',
	`type` varchar(50) NOT NULL,
	`phase` varchar(50) NOT NULL,
  `time_created` datetime NOT NULL DEFAULT 0,
	`time_start` datetime NOT NULL DEFAULT 0,
	`time_end` datetime NOT NULL DEFAULT 0,
  `user_agent` varchar(256) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `job_info` text NOT NULL DEFAULT '',
  `deleted` boolean NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`), UNIQUE KEY `job_id` (`job_id`)
)
