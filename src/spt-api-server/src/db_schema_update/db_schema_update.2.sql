#---
CREATE TABLE IF NOT EXISTS `user_blacklist`(
	`user_id` varchar(50) NOT NULL,
	`time_added` datetime NOT NULL,
	PRIMARY KEY (`user_id`), UNIQUE KEY `user_id` (`user_id`)
)
