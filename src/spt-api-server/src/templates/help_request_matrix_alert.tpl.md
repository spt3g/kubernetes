### SPT-3G Help Request

* Name: {{ name }}
* Email: [`{{ email }}`](mailto:{{ email }})
* Time: `{{ time }}`
* Nextcloud Deck card: [{{ card_url }}]({{ card_url }})
* Message:
  {{ message }}
