import os
import global_vars
from global_vars import STATUS_OK, config, log
import tornado.ioloop
import tornado.web
import tornado.template
import tornado.escape
from tornado.auth import OAuth2Mixin
import json
from datetime import datetime
import jinja2
import bcrypt
import time
from dbconnector import DbConnector
import re
from mimetypes import guess_type
from jwtutils import authenticated, encode_info, refresh_token, allowed_roles
import urllib.parse
from typing import Any, Dict
from io import StringIO
import pandas as pd
import math
from jobutils import valid_job_id, construct_job_object, delete_job_files
from cron import update_job_status
from email_utils import send_email
from keycloak import KeycloakApi
from email_utils import send_job_complete_email
from nextcloud import NextcloudDeckApi
from synapse import SynapseAdminApi
from markdown import markdown

# Get global instance of the job handler database interface
db = DbConnector(
    mysql_host=config['db']['host'],
    mysql_user=config['db']['user'],
    mysql_password=config['db']['pass'],
    mysql_database=config['db']['database'],
)

# Load Kubernetes API
try:
    import kubejob
except Exception as e:
    log.warning(f'''Failure loading Kubernetes client: {e}''')


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers",
                        "Origin, X-Requested-With, Content-Type, Accept, Authorization")
        self.set_header("Access-Control-Allow-Methods",
                        " POST, PUT, DELETE, OPTIONS, GET")

    def options(self):
        self.set_status(204)
        self.finish()

    def get_current_user(self):
        return self.get_secure_cookie("user")

    def getarg(self, arg, default=None):
        '''
        Calls to this function in BaseHandler.get(), BaseHandler.post(), etc must be surrounded by try/except blocks like so:

            try:
                ownerId = self.getarg('ownerId')
            except:
                self.finish()
                return

        '''
        value = default
        try:
            # If the request encodes arguments in JSON, parse the body accordingly
            if 'Content-Type' in self.request.headers and self.request.headers['Content-Type'] in ['application/json', 'application/javascript']:
                data = tornado.escape.json_decode(self.request.body)
                if default == None:
                    # The argument is required and thus this will raise an exception if absent
                    value = data[arg]
                else:
                    # Set the value to the default
                    value = default if arg not in data else data[arg]
            # Otherwise assume the arguments are in the default content type
            else:
                # The argument is required and thus this will raise an exception if absent
                if default == None:
                    value = self.get_argument(arg)
                else:
                    value = self.get_argument(arg, default)
        except Exception as e:
            response = str(e).strip()
            log.error(response)
            # 400 Bad Request: The server could not understand the request due to invalid syntax.
            # The assumption is that if a function uses `getarg()` to get a required parameter,
            # then the request must be a bad request if this exception occurs.
            self.send_response(response, http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            raise e
        return value

    # The datetime type is not JSON serializable, so convert to string
    def json_converter(self, o):
        if isinstance(o, datetime):
            return o.__str__()

    def send_response(self, data='', http_status_code=global_vars.HTTP_OK, return_json=True, indent=None):
        if return_json:
            if indent:
                self.write(json.dumps(data, indent=indent, default=self.json_converter))
            else:
                self.write(json.dumps(data, default=self.json_converter))
            self.set_header('Content-Type', 'application/json')
        else:
            self.write(data)
        self.set_status(http_status_code)


def renderJinjaTemplate(tpl_filename, values={}):
    text = ''
    path, filename = os.path.split(os.path.join(os.path.dirname(__file__), 'templates', tpl_filename))
    text = jinja2.Environment(loader=jinja2.FileSystemLoader(path or './')).get_template(filename).render(values)
    return text


def escape_html(htmlstring):
    escapes = {'\"': '&quot;',
               '\'': '&#39;',
               '<': '&lt;',
               '>': '&gt;'}
    # This is done first to prevent escaping other escapes.
    htmlstring = htmlstring.replace('&', '&amp;')
    for seq, esc in escapes.items():
        htmlstring = htmlstring.replace(seq, esc)
    return htmlstring


def get_api_base_path():
    ## Generate API base path for links
    basePath = f'''{config['server']['basePath']}/{config['server']['apiBasePath']}'''
    basePath = basePath.strip('/')
    return basePath


def import_positions_table(positions_csv):

    # Read in the positions as csv
    positions_df = None
    positions_list = positions_csv.split('\n')
    positions_list[0] = positions_list[0].lower()
    positions_csv_text_lowercase_column_headings = '\n'.join(positions_list)
    positions_df = pd.DataFrame(pd.read_csv(StringIO(positions_csv_text_lowercase_column_headings),
                                comment='#', skip_blank_lines=True, skipinitialspace=True))
    return positions_df


def export_positions_table(positions_df):

    # Get the header for the positions table
    position_columns = positions_df.columns

    # Build the list of positions
    position_list = []
    for row_index, cutout in positions_df.iterrows():
        d = {}
        for column in positions_df.columns:
            d[column] = cutout[column]
        position_list.append(d)
    return position_list, position_columns


def validate_positions_table(positions_df):
    msg = ''
    valid_column_headers = [
        'objid',
        'ra',
        'dec',
        'xsize',
        'ysize',
    ]
    # Check for invalid column headers
    invalid_column_headers = []
    for column_header in positions_df.columns:
        if column_header.lower() not in valid_column_headers:
            invalid_column_headers.append(column_header)
    if invalid_column_headers:
        return False, 'Invalid columns in positions table: {}'.format(invalid_column_headers)
    # Iterate over each position
    for row_index, cutout in positions_df.iterrows():
        if not ('ra' in cutout and not math.isnan(cutout['ra']) and 'dec' in cutout and not math.isnan(cutout['dec'])):
            return False, 'Each row must include RA/DEC coordinates.'
    return True, msg


def register_user(user_info):
    state = 'unregistered'
    user_id = user_info['user_id']
    ## Add record to register table
    registrant = db.select_registrants(user_id=user_id)
    if not registrant:
        try:
            db.insert_registrant(user_info)
            state = 'registered'
        except Exception as e:
            log.debug(f'''Error adding registrant to register table: {e}''')
            return state
    else:
        registrant = registrant[0]
        ## If registration has been approved, redirect to reauthenticate so that the new group
        if registrant['approved']:
            state = 'approved'
            return state
        else:
            state = 'registered'
            if registrant['email_sent']:
                log.debug('Registration approval email already sent.')
                return state
    ## Email admins for approval
    approve_url = f'''https://{os.path.join(config['server']['hostName'], config['server']['basePath'], 'account/register/approve', user_id)}'''
    email_subject = f'''SPT-3G Registration Received'''
    message_body = f'''
        <p>
            A user registration was received:
        </p>
        <p>
            <table>
                <tr><td>Name:</td><td><code>{user_info['name']}</code></td></tr>
                <tr><td>Email:</td><td><code>{user_info['email']}</code></td></tr>
                <tr><td>Keycloak ID:</td><td><code>{user_id}</code></td></tr>
            </table>
        </p>
        <p>
            <a href="{approve_url}">Click here to APPROVE.</a>
        </p>
        '''
    try:
        log.debug('Sending registration approval email...')
        send_email(recipients=config['email']['recipients'],
            email_subject=email_subject,
            message_body=message_body,
            template_file='email_base.tpl.html')
        db.mark_registrant_approval_email_sent(user_id=user_id)
    except Exception as e:
        log.debug(f'''Error sending registration approval email: {e}''')
    return state


class JobReportStartHandler(BaseHandler):
    def post(self, job_id=None):
        try:
            assert valid_job_id(job_id)
        except Exception as e:
            self.send_response("Invalid job ID", http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            self.finish()
            return
        try:
            token = self.getarg('token') # required
        except:
            self.finish()
            return
        try:
            assert token == config['jwt']['hs256Secret']
        except:
            self.send_response("Invalid auth token", http_status_code=global_vars.HTTP_UNAUTHORIZED, return_json=False)
            self.finish()
            return
        try:
            log.debug(f'''Updating started job "{job_id}"...''')
            db.update_job(
                job_id=job_id,
                phase='executing',
                start_time=datetime.utcnow(),
            )
        except Exception as e:
            log.error(f'''Error updating job "{job_id}" start time: {e}''')


class JobReportCompleteHandler(BaseHandler):
    def post(self, job_id=None):
        try:
            assert valid_job_id(job_id)
        except Exception as e:
            self.send_response("Invalid job ID", http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            self.finish()
            return
        try:
            token = self.getarg('token') # required
            phase = self.getarg('phase', 'completed') # optional
        except:
            self.finish()
            return
        try:
            assert token == config['jwt']['hs256Secret']
        except:
            self.send_response("Invalid auth token", http_status_code=global_vars.HTTP_UNAUTHORIZED, return_json=False)
            self.finish()
            return
        try:
            log.debug(f'''Updating completed job "{job_id}"...''')
            db.update_job(
                job_id=job_id,
                phase=phase,
                end_time=datetime.utcnow(),
                queue_position=-1,
            )
        except Exception as e:
            log.error(f'''Error updating job "{job_id}": {e}''')
        try:
            ## Delete Kubernetes objects associated with the job if it was aborted
            if phase == 'aborted':
                kubejob.delete_job(job_id=job_id)
        except Exception as e:
            log.error(f'''Error deleting Kubernetes Job objects: {e}''')
        try:
            log.debug(f'''Querying job record "{job_id}"...''')
            job_query = db.select_job_records(job_id=job_id, hasEmailAddress=True)
            if not job_query:
                return
            job = job_query[0]
            log.debug(f'''Sending email notification for "{job_id}"...''')
            send_job_complete_email(job=job)
            ## Remove the email address from the job record to mark as sent
            db.update_job(job_id=job_id, email='')
        except Exception as e:
            log.error(f'''Error sending job completion email "{job_id}": {e}''')


@authenticated
class JobHandler(BaseHandler):
    def put(self):
        try:
            positions = self.getarg('positions') # required
            email_notify = self.getarg('email', False) # optional
            date_start = self.getarg('date_start') # required
            date_end = self.getarg('date_end') # required
            ## TODO: Load default values from a single truth source coupled to the spt_cutter code repo
            xsize = self.getarg('xsize', 10.0) # optional
            ysize = self.getarg('ysize', 10.0) # optional
            bands = self.getarg('bands', ['150GHz']) # optional
            filetypes = self.getarg('filetypes', ['passthrough']) # optional
            yearly_coadd = self.getarg('yearly_coadd', ['yearly_winter_2019-2023']) # optional
            get_lightcurve = self.getarg('get_lightcurve', False) # optional
            nofits = self.getarg('nofits', False) # optional
            get_uniform_coverage = self.getarg('get_uniform_coverage', False) # optional

            # The number of cores:
            ncpu = config['uws']['job']['resources']['limits']['cpu']
            #np = int(0.5*float(ncpu))
            np = 0

            try:
                positions_df = import_positions_table(positions)
            except Exception as e:
                self.send_response(f'Error parsing positions table: {e}', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
            try:
                valid, msg = validate_positions_table(positions_df)
                if not valid:
                    self.send_response(f'Invalid positions table: {msg}', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                    self.finish()
                    return
            except Exception as e:
                self.send_response(f'Error validating positions table: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                self.finish()
                return

            position_list, position_columns = export_positions_table(positions_df)

            job_config = {
                'position_list': position_list,
                'position_columns': position_columns,
                'bands': bands,
                'filetypes': filetypes,
                'date_start': date_start,
                'date_end': date_end,
                'xsize': xsize,
                'ysize': ysize,
                'yearly_coadd': yearly_coadd,
                'get_lightcurve': get_lightcurve,
                'nofits': nofits,
                'get_uniform_coverage': get_uniform_coverage,
            }

            ## Command that the job container will execute. The `$JOB_OUTPUT_DIR` environment variable is
            ## populated at run time after a job ID and output directory have been provisioned.
            ## TODO: The spt3g_cutter command accepts a config file input instead of individual arguments.
            command = 'spt3g_cutter --inputList /etc/cutout/positions.csv'
            command = f'''{command} --date_start {job_config["date_start"]}'''
            command = f'''{command} --date_end {job_config["date_end"]}'''
            command = f'''{command} --bands {" ".join(job_config["bands"])}'''
            command = f'''{command} --yearly {" ".join(job_config["yearly_coadd"])}'''
            command = f'''{command} --dbname /data/spt3g/dblib/spt3g.db'''
            if nofits:
                command = f'''{command} --nofits'''
            if get_lightcurve:
                command = f'''{command} --get_lightcurve'''
            if get_uniform_coverage:
                command = f'''{command} --get_uniform_coverage'''
            command = f'''{command} --filetypes {" ".join(job_config["filetypes"])}'''
            command = f'''{command} --np {np}'''
            command = f'''{command} --stage'''
            command = f'''{command} --outdir $JOB_OUTPUT_DIR'''
            command = f'''{command} 2>&1 | tee $JOB_OUTPUT_DIR/job.log'''

            log.debug(f"The command is: {command}")

            ## Disabled options:
            #
            # # environment is a list of environment variable names and values like [{'name': 'env1', 'value': 'val1'}]
            environment = self.getarg('environment', default=[]) # optional
            # environment = []
            # Number of parallel job containers to run. The containers will execute identical code. Coordination is the
            # responsibility of the job owner.
            # replicas = self.getarg('replicas', default=1) # optional
            replicas = 1
            # # The URL of the git repo to clone
            # url = self.getarg('url', default='') # optional
            url = ''
            # # The git reference (branch name or commit hash) to be checked out after cloning the git repo
            # commit_ref = self.getarg('commit_ref', default='') # optional
            commit_ref = ''


            # Valid run_id value follows the Kubernetes label value constraints:
            #   - must be 63 characters or less (cannot be empty),
            #   - must begin and end with an alphanumeric character ([a-z0-9A-Z]),
            #   - could contain dashes (-), underscores (_), dots (.), and alphanumerics between.
            # See also:
            #   - https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#runId
            #   - https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
            run_id = self.getarg('run_id', default='') # optional
            if run_id and (not isinstance(run_id, str) or run_id != re.sub(r'[^-._a-zA-Z0-9]', "", run_id) or not re.match(r'[a-zA-Z0-9]', run_id)):
                self.send_response('Invalid run_id. Must be 63 characters or less and begin with alphanumeric character and contain only dashes (-), underscores (_), dots (.), and alphanumerics between.', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
            ## Obtain user ID from auth token
            user_id = self._token_decoded["user_id"]
        except Exception as e:
            self.send_response(
                str(e), http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return

        response = kubejob.create_job(
            command=command,
            run_id=run_id,
            owner_id=user_id,
            replicas=replicas,
            environment=environment,
            url=url,
            commit_ref=commit_ref,
            job_config=job_config,
        )
        # log.debug(response)
        if response['status'] != global_vars.STATUS_OK:
            self.send_response(response['message'], http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        try:
            timeout = 30
            while timeout > 0:
                results = kubejob.list_jobs(
                    job_id=response['job_id'],
                )
                if results['jobs']:
                    job = construct_job_object(results['jobs'][0])
                    try:
                        user_agent = self.request.headers["User-Agent"]
                    except:
                        user_agent = ''
                    ## Obtain user email address from auth token
                    email = ''
                    if email_notify:
                        try:
                            email = self._token_decoded["email"]
                        except:
                            log.error('Error obtaining email address from decoded auth token.')
                    queue_position = 0
                    try:
                        ## TODO: Replace with specialized query that uses the SQL count() command
                        job_list = []
                        for phase in ['pending', 'queued', 'executing']:
                            job_list.extend(db.select_job_records(phase=phase, fields=['id']))
                        log.debug(job_list)
                        queue_position = len(job_list)
                    except Exception as e:
                        log.error(f'''Error querying job queue position: {e}''')
                    try:
                        command = job['parameters']['command']
                        if isinstance(command, list):
                            command = ' '.join(command)
                        ## TODO: Replace the JSON dump of the full job info with proper table records.
                        job_info = {
                            'job_id': job['jobId'],
                            'run_id': job['runId'],
                            'user_id': job['ownerId'],
                            'command': command,
                            'type': 'cutout',
                            'phase': job['phase'],
                            'time_created': job['creationTime'] or 0,
                            'time_start': job['startTime'] or 0,
                            'time_end': job['endTime'] or 0,
                            'user_agent': user_agent,
                            'email': email,
                            'job_info': json.dumps(job, default = self.json_converter),
                            'positions': json.dumps(position_list, default = self.json_converter),
                            'queue_position': queue_position,
                            }
                        db.insert_job_record(job_info)
                    except Exception as e:
                        err_msg = f'''Error recording job in database: {e}'''
                        log.error(err_msg)
                        self.send_response(err_msg, http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                        self.finish()
                        return
                    self.send_response(job, indent=2)
                    self.finish()
                    return
                else:
                    timeout -= 1
                    time.sleep(0.300)
            self.send_response("Job creation timed out.", http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        except Exception as e:
            self.send_response(str(e), http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return

    def get(self, job_id=None, property=None):
        # See https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#resourceuri
        ## The valid properties are the keys of the returned job data structure
        valid_properties = {
            'phase': 'phase',
            'user_id': 'results',
            'command': 'command',
            'run_id': 'run_id',
            'email': 'email',
            'type': 'type',
            'job_info': 'job_info',
        }
        response = {}
        # If no job_id is included in the request URL, return a list of jobs. See:
        # UWS Schema: https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#UWSSchema
        if not job_id:
            phase = self.getarg('phase', default='') # optional
            if not phase or phase in global_vars.VALID_JOB_STATUSES:
                # results = kubejob.list_jobs()
                try:
                    ## Query for all jobs belonging to the authenticated user
                    # update_job_status(user_id=self._token_decoded["user_id"])
                    job_list = db.select_job_records(user_id=self._token_decoded["user_id"], phase=phase)
                    self.send_response(job_list, indent=2)
                    self.finish()
                    return
                except Exception as e:
                    self.send_response(f'''Error querying job records: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
            else:
                response = 'Valid job categories are: {}'.format(global_vars.VALID_JOB_STATUSES)
                self.send_response(response, http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
        # If a job_id is provided but it is invalid, then the request is malformed:
        if not valid_job_id(job_id):
            self.send_response('Invalid job ID.', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
            self.finish()
            return
        # If a property is provided but it is invalid, then the request is malformed:
        elif isinstance(property, str) and property not in valid_properties:
            self.send_response(f'Invalid job property requested. Supported properties are {", ".join([key for key in valid_properties.keys()])}', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
            self.finish()
            return
        try:
            ## Query the specified job by ID
            # update_job_status(job_id=job_id)
            job_list = db.select_job_records(job_id=job_id)
            try:
                job = job_list[0]
            except:
                job = {}
            ## TODO: Implement the specific property return if requested
            ## If a specific job property was requested using an API endpoint
            ## of the form `/job/[job_id]/[property]]`, return that property only.
            if property and property in valid_properties.keys():
                job = job[valid_properties[property]]
            self.send_response(job, indent=2)
            self.finish()
            return
        except Exception as e:
            self.send_response(f'''Error querying job records: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return

    def delete(self, job_id=None):
        try:
            assert valid_job_id(job_id)
        except Exception as e:
            self.send_response("Invalid job ID", http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            self.finish()
            return
        response = ''
        try:
            ## Mark job deleted in `job` table
            db.mark_job_deleted(job_id)
        except Exception as e:
            log.error(f'''Error marking job deleted: {e}''')
            response += 'Error marking job deleted. '
        try:
            ## Delete Kubernetes objects associated with the job
            kubejob.delete_job(job_id=job_id)
        except Exception as e:
            log.error(f'''Error deleting Kubernetes Job objects: {e}''')
            response += 'Error deleting Kubernetes objects. '
        ## TODO: 2021/12/01 Deleting jobs is so slow that it causes the API
        ##       server to become unresponsive while deleting job files. As
        ##       a workaround, leave actual file deletion to the garbage
        ##       collection CronJob.
        # try:
        #     ## Delete files associated with the job
        #     assert delete_job_files(job_id)
        # except Exception as e:
        #     log.error(f'''Error deleting job files: {e}''')
        #     response += 'Error deleting job files. '
        if response:
            self.send_response(response, http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        log.debug(f'''Job "{job_id}" deleted successfully.''')
        self.send_response()
        self.finish()
        return


class ResultFileHandler(BaseHandler):
    def get(self, job_id=None, result_id=None):
        try:
            # If a job_id is provided but it is invalid, then the request is malformed:
            if not valid_job_id(job_id):
                self.send_response('Invalid job ID.', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
                self.finish()
                return
            # If a result_id is not provided, then the request is malformed:
            if not result_id:
                self.send_response('Invalid result ID.', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
                self.finish()
                return
            try:
                result_idx = int(result_id)
                job_files = kubejob.list_job_output_files(job_id)
                file_path = job_files[result_idx]
            except:
                self.send_response('Result file not found.', http_status_code=global_vars.HTTP_NOT_FOUND, return_json=False)
                self.finish()
                return
            if not os.path.isfile(file_path):
                self.send_response('Result file not found.', http_status_code=global_vars.HTTP_NOT_FOUND, return_json=False)
                self.finish()
                return
            # TODO: Consider applying "application/octet-stream" universally given the error rate with the guess_type() function
            content_type, _ = guess_type(file_path)
            if not content_type:
                content_type = "application/octet-stream"
            self.add_header('Content-Type', content_type)
            with open(file_path, 'rb') as source_file:
                self.send_response(source_file.read(), return_json=False)
                self.finish()
                return
        except Exception as e:
            response = str(e).strip()
            log.error(response)
            self.send_response(response, http_status_code=global_vars.HTTP_SERVER_ERROR, indent=2)
            self.finish()
            return


class AccountHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
        html = loader.load("account.html").generate(
            project_name=config['project']['name'],
            profile_url=config['oidc']['profileUrl'],
            user_info=json.loads(self.get_current_user()),
            base_path=config['server']['basePath'],
            base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
            unauthorized=False,
        )
        self.send_response(html, return_json=False)
        self.finish()


class ProfileHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        try:
            user_info = json.loads(self.get_current_user())
        except Exception as e:
            self.send_response(f'Error getting user info: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        response = {
            'email': user_info['email'],
            'preferred_username': user_info['preferred_username'],
            'given_name': user_info['given_name'],
            'family_name': user_info['family_name'],
            'name': user_info['name'],
            'groups': user_info['groups'],
        }
        self.send_response(response)
        self.finish()

@authenticated
class HelpFormHandler(BaseHandler):
    def put(self):
        try:
            raw_message = self.getarg('message')
            message = escape_html(raw_message) # required
        except:
            self.finish()
            return
        try:
            ## Obtain user info from auth token
            name = self._token_decoded["name"]
            email = self._token_decoded["email"]
            ##
            ## Create Nextcloud Deck card
            ##
            try:
                ## Create a new card in the Nextcloud Deck
                card_title = f'''Help request ({datetime.today().strftime('%Y-%m-%d')}): {name}'''
                time_stamp = f'''{datetime.today().strftime('%Y-%m-%d %H:%M:%S')} UTC'''
                loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
                card_description = loader.load("help_request_card_description.tpl.md").generate(
                    name=name,
                    email=email,
                    time=time_stamp,
                    message=raw_message,
                ).decode('utf-8')
                log.debug(f'''{card_description}''')
                deck_api = NextcloudDeckApi(
                    base_url=config['nextcloud']['base_url'],
                    auth_user=config['nextcloud']['auth_user'],
                    auth_token=config['nextcloud']['auth_token'],
                )
                ## Find the IDs of the target board and stack
                stack_id = board_id = None
                boards = deck_api.get_deck_boards()
                for board in boards:
                    if board['title'] == config['nextcloud']['deck']['board_title']:
                        board_id = board['id']
                        # log.debug(f'''Board ID: {board_id}''')
                        stacks = deck_api.get_deck_stacks(board_id=board_id)
                        for stack in stacks:
                            # log.debug(f'''Stack: {stack['title']}''')
                            if stack['title'] == config['nextcloud']['deck']['stack_title']:
                                stack_id = stack['id']
                                # log.debug(f'''Stack ID: {stack_id}''')
                ## Create card
                assert stack_id and board_id
                new_card = deck_api.deck_create_card(
                    board_id=board_id,
                    stack_id=stack_id,
                    card_title=card_title,
                    card_description=card_description,
                )
                log.debug(f'''New Deck card: {json.dumps(new_card, indent=2)}''')
                assigned_card = deck_api.deck_card_assign_user(
                    board_id=board_id,
                    stack_id=stack_id,
                    card_id=new_card['id'],
                    user_id=config['nextcloud']['deck']['default_assignee'],
                )
                log.debug(f'''Card assigned: {json.dumps(assigned_card, indent=2)}''')
                card_url = f'''{config['nextcloud']['base_url']}/apps/deck/#/board/{board_id}/card/{new_card['id']}'''
            except Exception as e:
                log.error(f'''Error creating Nextcloud Deck card for help request: {e}''')
                card_url = ''
            ##
            ## Send notification via email
            ##
            admin_emails = config['email']['recipients']
            if isinstance(admin_emails, str):
                admin_emails = [admin_emails]
            assert isinstance(admin_emails, list)
            admin_emails = set(admin_emails)
            recipients = list(admin_emails.union(set([email])))
            message_body = f'''
                <p>Help request received. Please reply-all to this email when replying.</p>
                <p><table>
                    <tr><td>Name:</td><td>{name}</td></tr>
                    <tr><td>Email:</td><td>{email}</td></tr>
                    <tr><td>Nextcloud Deck card:</td><td><a href="{card_url}">{card_url}</a></td></tr>
                    <tr><td></td><td><p>{message}</p></td></tr>
                </table></p>
            '''
            try:
                send_email(recipients=recipients,
                        email_subject='''SPT-3G Help Request''',
                        message_body=message_body,
                        template_file='email_base.tpl.html')
            except Exception as e:
                log.error(f'''Error sending help request email: {e}''')
            ##
            ## Send notification via Matrix
            ##
            try:
                alert_message = loader.load("help_request_matrix_alert.tpl.md").generate(
                    name=name,
                    email=email,
                    time=time_stamp,
                    message=raw_message,
                    card_url=card_url,
                ).decode('utf-8')
                synapse_api = SynapseAdminApi({
                    'server_url': config['matrix']['base_url'],
                    'auth_token': config['matrix']['auth_token'],
                })
                msg_sent = synapse_api.send_message(
                    room_id=config['matrix']['room_id'],
                    message_body=alert_message,
                    message_body_html=markdown(alert_message),
                )
                assert msg_sent
            except Exception as e:
                log.error(f'''Error creating alert via Matrix for help request: {e}''')

            self.send_response()
            self.finish()
            return
        except Exception as e:
            self.send_response(f'Error sending help message: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return


class AccountTokenCreateHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        try:
            user_info = json.loads(self.get_current_user())
            log.debug(json.dumps(user_info, indent=2))
            token = encode_info(user_info).decode(encoding='utf-8')
        except Exception as e:
            self.send_response(f'Error getting user info: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
        html = loader.load("token.tpl.html").generate(
            project_name=config['project']['name'],
            profile_url=config['oidc']['profileUrl'],
            user_info=json.loads(self.get_current_user()),
            base_path=config['server']['basePath'],
            base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
            unauthorized=False,
            api_base_url=f'''{config['server']['hostName']}/{get_api_base_path()}''',
            ttl=round(config['jwt']['ttlSeconds']/3600.0),
            token=token,
        )
        self.send_response(html, return_json=False)
        self.finish()
    @tornado.web.authenticated
    def post(self):
        try:
            user_info = json.loads(self.get_current_user())
            token = encode_info(user_info).decode(encoding='utf-8')
        except Exception as e:
            self.send_response(f'Error getting user info: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        response = {
            'token': token,
            'maxAge': config['jwt']['ttlSeconds'],
            'userInfo': {
                'email': user_info['email'],
                'preferred_username': user_info['preferred_username'],
                'given_name': user_info['given_name'],
                'family_name': user_info['family_name'],
                'name': user_info['name'],
                'groups': user_info['groups'],
            }
        }
        self.send_response(response)
        self.finish()


@authenticated
class AccountTokenRefreshHandler(BaseHandler):
    def get(self):
        try:
            auth = self.request.headers.get("Authorization")
            parts = auth.split()
            auth_type = parts[0].lower()
            assert auth_type == 'bearer' and len(parts)==2
            token = parts[1]
        except Exception as e:
            self.send_response(f'Invalid authorization header: {e}', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            self.finish()
            return
        response = refresh_token(token)
        if response['status'] != STATUS_OK:
            self.send_response(f'''Error refreshing token: {response['message']}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        new_token = response['token']
        data = {
            'token': new_token,
        }
        self.send_response(data)
        self.finish()


class RegisterApproveHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, user_id):
        try:
            user_info = json.loads(self.get_current_user())
            if '/admin' not in user_info['groups']:
                self.send_response(f'''This action requires admin privileges.''', http_status_code=global_vars.HTTP_UNAUTHORIZED, return_json=False)
                self.finish()
                return
        except Exception as e:
            self.send_response(f'''Error authorizing admin: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        try:
            registrants = db.select_registrants(user_id=user_id)
            if registrants:
                error = False
                registrant_info = registrants[0]
                ## Update the register table record
                db.mark_registrant_approved(user_id=user_id)


                ## Add the user to the relevant Keycloak groups
                keycloak_client = KeycloakApi()
                email = registrant_info['email']
                log.debug(f'''email: {email}''')
                keycloak_user = keycloak_client.get_user_info(email=email)
                log.debug(f'''keycloak_user: {keycloak_user}''')
                if not keycloak_user:
                    self.send_response({'msg': f'''No account with email "{email}" found.'''}, http_status_code=global_vars.HTTP_NOT_FOUND, return_json=True)
                    self.finish()
                    return
                keycloak_user = keycloak_user[0]
                log.debug(f'''Keycloak user info: "{keycloak_user}"''')
                log.debug(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has been approved.''')
                ## Add user to Keycloak Collaborator role if they are not already
                user_groups = keycloak_client.get_user_group_membership(keycloak_user['id'])
                log.debug(f'''user_groups: "{json.dumps(user_groups, indent=2)}"''')
                if [group for group in user_groups if group['name'] == '/spt-default']:
                    self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is already in the spt-default group.''', return_json=False)
                    self.finish()
                    return
                success = keycloak_client.add_user_to_group(keycloak_user['id'], group_name='spt-default')
                if not success:
                    self.send_response(f'''Error adding "{keycloak_user['firstName']} {keycloak_user['lastName']}" to spt-default group.''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
                ## Send user an email confirming their approved account
                cutout_url = f'''https://{os.path.join(config['server']['hostName'], config['server']['basePath'], 'app/cutout')}'''
                account_url = f'''https://{os.path.join(config['server']['hostName'], config['server']['basePath'], 'account')}'''
                email_subject = f'''SPT-3G Account Approved'''
                message_body = f'''
                    <p>
                        Your SPT-3G account has been approved! You may now access the SPT-3G services:
                    </p>
                    <p>
                        <ul>
                            <li><a href="{account_url}">Account Home</a></li>
                            <li><a href="{cutout_url}">Cutout Service</a></li>
                        </ul>
                    </p>
                    '''
                send_email(recipients=email,
                    email_subject=email_subject,
                    message_body=message_body,
                    template_file='email_base.tpl.html')
            else:
                error = True
                registrant_info = {'user_id': user_id}
            loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
            html = loader.load("registration_approved.tpl.html").generate(
                project_name=config['project']['name'],
                profile_url=config['oidc']['profileUrl'],
                user_info=user_info,
                base_path=config['server']['basePath'],
                base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
                unauthorized=False,
                api_base_url=f'''{config['server']['hostName']}/{get_api_base_path()}''',
                error=error,
                registrant_info=registrant_info,
            )
            self.send_response(html, return_json=False)
            self.finish()
        except Exception as e:
            self.send_response(f'''Error approving registrant "{user_id}": {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()

@authenticated
@allowed_roles(['api_admin'])
class AdminTokenInvalidateHandler(BaseHandler):
    def get(self):
        try:
            denylist = db.get_denylist()
            self.send_response(denylist)
            self.finish()
        except Exception as e:
            self.send_response(f'''Error fetching denylist: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
    def put(self):
        try:
            ## Keycloak user ID
            user_id = self.getarg('uid') # required
        except Exception as e:
            self.finish()
            return
        try:
            db.update_denylist(user_id, 'add')
            self.send_response()
            self.finish()
        except Exception as e:
            self.send_response(f'''Error adding user "{user_id}" to denylist: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
    def delete(self):
        try:
            ## Keycloak user ID
            user_id = self.getarg('uid') # required
        except Exception as e:
            self.finish()
            return
        try:
            db.update_denylist(user_id, 'remove')
            self.send_response()
            self.finish()
        except Exception as e:
            self.send_response(f'''Error deleting user "{user_id}" from denylist: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()


class KeycloakOAuth2Mixin(OAuth2Mixin):
    '''ref: https://www.tornadoweb.org/en/stable/auth.html'''

    _OAUTH_AUTHORIZE_URL = config['oidc']['authorizeUrl']
    _OAUTH_ACCESS_TOKEN_URL = config['oidc']['tokenUrl']
    _OAUTH_SETTINGS_KEY = {
        'key': config['oidc']['clientId'],
        'secret': config['oidc']['clientSecret'],
    }
    _OAUTH_USERINFO_URL = config['oidc']['userInfoUrl']
    _OAUTH_NO_CALLBACKS = False

    async def get_authenticated_user(self, redirect_uri: str, code: str) -> Dict[str, Any]:
        http = self.get_auth_http_client()
        body = urllib.parse.urlencode(
            {
                "redirect_uri": redirect_uri,
                "code": code,
                "client_id": self._OAUTH_SETTINGS_KEY["key"],
                "client_secret": self._OAUTH_SETTINGS_KEY["secret"],
                "grant_type": "authorization_code",
            }
        )

        response = await http.fetch(
            self._OAUTH_ACCESS_TOKEN_URL,
            method="POST",
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            body=body,
        )
        return tornado.escape.json_decode(response.body)


    async def get_user_info(self, access_token):
        http = self.get_auth_http_client()

        response = await http.fetch(
            self._OAUTH_USERINFO_URL,
            method="GET",
            headers={"Authorization": f'''Bearer {access_token}'''},
        )
        return tornado.escape.json_decode(response.body)


class LoginHandler(BaseHandler, KeycloakOAuth2Mixin):
    '''ref: https://www.tornadoweb.org/en/stable/auth.html'''
    async def get(self):
        default_path = f"/{config['server']['basePath']}/account"
        default_path = f"/{default_path.strip('/')}"
        if self.get_argument('code', False):
            # default_path = f"/{config['server']['basePath']}/account"
            # next_path = self.get_argument('dest', default_path)
            # if not self.next_path:
            #     self.next_path = default_path
            log.debug(self.get_argument('code'))
            auth_token = await self.get_authenticated_user(
                redirect_uri=f"{config['oidc']['callbackUrl']}",
                code=self.get_argument('code')
            )
            log.debug(json.dumps(auth_token))
            user_info = await self.get_user_info(
                access_token=auth_token['access_token'],
            )
            ## Add user_id key if "sub" is provided by the oauth token
            try:
                user_id = user_info['user_id']
            except:
                user_id = user_info['sub']
                user_info['user_id'] = user_id
            log.debug(json.dumps(user_info))
            ##
            ## Automatic account registration. If an authenticated user is not in the necessary access control groups, automatically
            ## trigger the registration flow.
            ##
            if not [group for group in config['oidc']['groups']['allowed'] if group in user_info[config['oidc']['groups']['keyName']]]:
                try:
                    error = False
                    ## Register user and send email to admins for approval
                    state = register_user(user_info)
                    if state == "approved":
                        ## If the user has already been approved, but they are reaching this point, it is because they are still
                        ## authenticated in the original session and have not acquired a fresh auth token that includes their
                        ## new group membership(s). Log them out and redirect back to their original destination.
                        self.clear_cookie("user")
                        ## Log out of Keycloak as well
                        dest_path = self.get_argument('next', default_path)
                        dest_url = f'''https://{config['server']['hostName']}{dest_path}'''
                        log.debug(dest_url)
                        self.set_cookie('dest_url', dest_url)
                        redirect_uri = urllib.parse.quote_plus(dest_url)
                        self.redirect(f'''{config['oidc']['logoutUrl']}?redirect_uri={redirect_uri}''')
                        return
                    elif state == "unregistered":
                        ## If the registration failed for some reason, note the error in the page displayed.
                        error = True
                    ## Otherwise, the registration was successful and the page will provide confirmation that the registration is
                    ## awaiting approval.
                except Exception as e:
                    error = True
                    log.error(f'Error registering user {json.dumps(user_info)}: {e}')
                loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
                html = loader.load("registration_received.tpl.html").generate(
                    project_name=config['project']['name'],
                    profile_url=config['oidc']['profileUrl'],
                    user_info=user_info,
                    base_path=config['server']['basePath'],
                    base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
                    api_base_url=f'''{config['server']['hostName']}/{get_api_base_path()}''',
                    unauthorized=True,
                    error=error,
                )
                self.send_response(html, return_json=False)
                self.finish()
                return
            # Save the user and access token
            self.set_secure_cookie("user", json.dumps(user_info))
            try:
                dest_url = self.get_cookie('dest_url')
            except:
                dest_url = default_path
            ## Redirect to the original desired URL or fall back to the default
            self.redirect(dest_url)
        else:
            # self.next_path = self.get_argument('next', default_path)
            dest_url = self.get_argument('next', default_path)
            self.set_cookie('dest_url', dest_url)
            self.authorize_redirect(
                redirect_uri=f"{config['oidc']['callbackUrl']}", #?dest={next_path}",
                client_id=config['oidc']['clientId'],
                client_secret=config['oidc']['clientSecret'],
                scope=config['oidc']['scope'],
                # extra_params={'dest': self.get_argument('next', f"{config['server']['basePath']}/account")},
            )


class LogoutHandler(BaseHandler):
    def get(self):
        # Clear the browser cookie
        self.clear_cookie("user")
        # Log out of Keycloak as well
        redirect_uri = urllib.parse.quote_plus(f'''https://{config['server']['hostName']}/{config['server']['basePath']}''')
        self.redirect(f'''{config['oidc']['logoutUrl']}?redirect_uri={redirect_uri}''')


def make_app(app_base_path='/', api_base_path='api', debug=False):
    ## Configure app base path
    app_base_path = f'''/{app_base_path.strip('/')}'''
    if app_base_path == '/':
        app_base_path = ''
    ## Configure API base path
    api_base_path = api_base_path.strip('/')
    settings = {
        "debug": debug,
        "login_url": r"{}/account/login".format(app_base_path),
        "cookie_secret": bcrypt.gensalt().decode('utf-8'),
        "static_path": os.path.join(os.path.dirname(os.path.abspath(__file__)), "static"),
    }
    settings['public_path'] = os.path.join(settings['static_path'], "app/public")
    return tornado.web.Application(
        [
            (r"{}/account/login".format(app_base_path), LoginHandler),
            (r"{}/account/logout".format(app_base_path), LogoutHandler),
            (r"{}/account/token/create".format(app_base_path), AccountTokenCreateHandler),
            (r"{}/account/*".format(app_base_path), AccountHandler),
            (r"{}/app/public/(.*)".format(app_base_path), tornado.web.StaticFileHandler, {'path': settings['public_path']}),
            (r"{}/{}/uws/job/result/(.*)/(.*)".format(app_base_path, api_base_path), ResultFileHandler),
            (r"{}/{}/uws/job/(.*)/(.*)".format(app_base_path, api_base_path), JobHandler),
            (r"{}/{}/uws/job/(.*)".format(app_base_path, api_base_path), JobHandler),
            (r"{}/{}/uws/job".format(app_base_path, api_base_path), JobHandler),
            (r"{}/{}/uws/report/start/(.*)".format(app_base_path, api_base_path), JobReportStartHandler),
            (r"{}/{}/uws/report/end/(.*)".format(app_base_path, api_base_path), JobReportCompleteHandler),
            (r"{}/{}/account/token/refresh".format(app_base_path, api_base_path), AccountTokenRefreshHandler),
            (r"{}/{}/admin/token/invalidate".format(app_base_path, api_base_path), AdminTokenInvalidateHandler),
            (r"{}/{}/profile".format(app_base_path, api_base_path), ProfileHandler),
            (r"{}/{}/help".format(app_base_path, api_base_path), HelpFormHandler),
            (r"{}/account/register/approve/(.*)".format(app_base_path), RegisterApproveHandler),
        ],
        **settings
    )


if __name__ == "__main__":
    try:
        # Wait for database to come online if it is still starting
        waiting_for_db = True
        while waiting_for_db:
            try:
                db.open_db_connection()
                waiting_for_db = False
                db.close_db_connection()
            except:
                log.error('Unable to connect to database. Waiting to try again...')
                time.sleep(5.0)
        ## Create/update database tables
        db.update_db_tables()
    except Exception as e:
        log.error(str(e).strip())
    ## Create TornadoWeb application
    app = make_app(
        app_base_path=config['server']['basePath'],
        api_base_path=config['server']['apiBasePath'],
        debug=config['server']['debug']
    )
    app.listen(int(config['server']['port']))
    log.info(f'''API server listening on port {config['server']['port']} at base path "{config['server']['basePath']}"''')
    if config['devMode']['enabled']:
        log.debug(json.dumps(config, indent=2))
    tornado.ioloop.IOLoop.current().start()
