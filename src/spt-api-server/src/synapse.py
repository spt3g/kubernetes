import sys, os
import requests
import json
import pathlib
import hmac, hashlib
try:
    from global_vars import config
except:
    config = None

import logging

# Configure logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger("spt-api")
# handler = logging.StreamHandler(sys.stdout)
# handler.setLevel(logging.DEBUG)
# formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
# handler.setFormatter(formatter)
# log.addHandler(handler)
try:
    log.setLevel(config['server']['logLevel'].upper())
except:
    log.setLevel('DEBUG')


def print_http_reponse(response):
    try:
        log.debug(f'Response: [{response.status_code}]\n{json.dumps(response.json(), indent=2)}')
    except:
        log.debug(f'Response: [{response.status_code}]\n{response.text}')


class SynapseAdminApi(object):
    """
    https://github.com/matrix-org/synapse/blob/master/docs/admin_api/
    """
    def __init__(self, conf):
        self.conf = conf
        self.json_header = {
            'Authorization': 'Bearer {}'.format(self.conf['auth_token']),
            'Content-Type': 'application/json',
        }
        self.event_token = ''
        # Get room list
        self.all_rooms = []

    def list_room_media(self, room_id):
        log.debug('Room "{}" media:'.format(room_id))
        response = requests.get(
            '{}/_synapse/admin/v1/room/{}/media'.format(self.conf['server_url'], room_id),
            headers=self.json_header
        )
        data = json.loads(response.text)
        log.debug(json.dumps(data, indent=2))
        if response.status_code != 200:
            log.error('ERROR: {}'.format(response.status_code))
            sys.exit()
        return data

    def purge_history(self, room_id, purge_up_to_ts):
        log.debug('Purging room "{}" history up to {} ...'.format(room_id, purge_up_to_ts))
        response = requests.post(
            '{}/_synapse/admin/v1/purge_history/{}'.format(self.conf['server_url'], room_id),
            json={
                'delete_local_events': False,
                'purge_up_to_ts': purge_up_to_ts,
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        log.debug(json.dumps(data, indent=2))
        if response.status_code != 200:
            log.error('ERROR: {}'.format(response.status_code))
            sys.exit()

    def list_all_rooms(self):
        response = requests.get(
            '{}/_synapse/admin/v1/rooms'.format(self.conf['server_url']),
            params={
                'limit': 10000
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        log.debug(json.dumps(data, indent=2))
        self.all_rooms = data['rooms']
        return data['rooms'], data['total_rooms']

    def event_reports(self, room_id):
        '''
        limit: integer - Is optional but is used for pagination, denoting the maximum number of items to return in this call. Defaults to 100.
        from: integer - Is optional but used for pagination, denoting the offset in the returned results. This should be treated as an opaque value and not explicitly set to anything other than the return value of next_token from a previous call. Defaults to 0.
        dir: string - Direction of event report order. Whether to fetch the most recent first (b) or the oldest first (f). Defaults to b.
        user_id: string - Is optional and filters to only return users with user IDs that contain this value. This is the user who reported the event and wrote the reason.
        room_id: string - Is optional and filters to only return rooms with room IDs that contain this value.

        '''
        response = requests.get(
            '{}/_synapse/admin/v1/event_reports'.format(self.conf['server_url']),
            params={
                'limit': 10,
                'room_id': room_id,
                'dir': 'f',
                'from': 0,
                # 'user_id': '',
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        return data

    def get_room_messages(self, room_id, direction = 'f', message_limit = 10):
        '''
        https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-rooms-roomid-messages

        Parameter 	Type 	Description
        path parameters
        roomId 	string 	Required. The room to get events from.
        query parameters
        from 	string 	Required. The token to start returning events from. This token can be obtained from a prev_batch token returned for each room by the sync API, or from a start or end token returned by a previous request to this endpoint.
        to 	string 	The token to stop returning events at. This token can be obtained from a prev_batch token returned for each room by the sync endpoint, or from a start or end token returned by a previous request to this endpoint.
        dir 	enum 	Required. The direction to return events from. One of: ["b", "f"]
        limit 	integer 	The maximum number of events to return. Default: 10.
        filter 	string 	A JSON RoomEventFilter to filter returned events with.
        '''
        response = requests.get(
            '{server_url}/_matrix/client/r0/rooms/{roomId}/messages'.format(server_url=self.conf['server_url'], roomId=room_id),
            params={
                'dir': direction,
                'from': self.event_token,
                # 'to': '',
                'limit': message_limit,
                # 'filter': '',
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        # Set the event_token for the next message history request based on the seeking direction
        if direction == 'f' and 'start' in data:
            self.event_token = data['start']
        elif 'end' in data:
            self.event_token = data['end']
        return data
        
    def get_all_users(self, include_guests=False):
        response = requests.get(
            f'''{self.conf['server_url']}/_synapse/admin/v2/users?from=0&guests={"true" if include_guests else "false"}''',
            params={
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        return data

    def update_user(self, user_id, admin=False):
        response = requests.put(
            f'''{self.conf['server_url']}/_synapse/admin/v2/users/{user_id}''',
            json={
                'admin': admin,
            },
            headers=self.json_header
        )
        return response

    def client_sync(self):
        '''
        https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-sync
        '''
        response = requests.get(
            '{server_url}/_matrix/client/r0/sync'.format(server_url=self.conf['server_url']),
            params={
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        log.debug('next_batch: {}'.format(data['next_batch']))
        self.event_token = data['next_batch']
        return data

    def purge_unused_rooms(self):
        all_rooms, total_room_number = self.list_all_rooms()
        room_ids_to_purge = []
        for room_id in all_rooms:
            if room_id['joined_local_members'] == 0:
                room_ids_to_purge.append(room_id['room_id'])
        log.debug(json.dumps(room_ids_to_purge, indent=2))
        log.debug('Rooms to purge: {}'.format(len(room_ids_to_purge)))

        for room_id in room_ids_to_purge:
            log.debug('Purging room: {}...'.format(room_id))
            response = requests.post(
                '{}/_synapse/admin/v1/purge_room'.format(self.conf['server_url']),
                json={
                    'room_id': room_id
                },
                headers=self.json_header
            )
            data = json.loads(response.text)
            log.debug(json.dumps(data, indent=2))
            if response.status_code != 200:
                log.error('ERROR: {}'.format(response.status_code))
                sys.exit()
        log.debug('Room purge complete.')
    
    def download_file(self, url, filename=None, dest_dir=None):
        if not filename:
            filename = url.split('/')[-1]
        if not dest_dir:
            dest_dir = pathlib.Path().resolve()
        os.makedirs(dest_dir, exist_ok=True)
        filepath = os.path.join(dest_dir, filename)
        tmpfilepath = os.path.join(dest_dir, f'''.tmp.{filename}''')
        if os.path.isfile(filepath):
            log.debug(f'''File already downloaded: "{filepath}".''')
        else:
            log.debug(f'''Download file to "{filepath}"...''')
            # NOTE the stream=True parameter below
            with requests.get(url, stream=True) as r:
                r.raise_for_status()
                with open(tmpfilepath, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192): 
                        # If you have chunk encoded response uncomment if
                        # and set chunk_size parameter to None.
                        #if chunk: 
                        f.write(chunk)
            os.rename(tmpfilepath, filepath)
        return filepath

    def reset_password(self, user_id, new_password, logout_devices=True):
        '''
        ref: https://matrix-org.github.io/synapse/latest/admin_api/user_admin_api.html#reset-password
        '''
        response = requests.post(
            f'''{self.conf['server_url']}/_synapse/admin/v1/reset_password/{user_id}''',
            json={
                'new_password': new_password,
                'logout_devices': logout_devices,
            },
            headers=self.json_header
        )
        print_http_reponse(response)
        try:
            data = json.loads(response.text)
        except:
            data = {}
        return data

    def list_pushers(self, user_id):
        '''
        ref: https://matrix-org.github.io/synapse/latest/admin_api/user_admin_api.html#list-all-pushers
        '''
        response = requests.get(
            f'''{self.conf['server_url']}/_synapse/admin/v1/users/{user_id}/pushers''',
            headers=self.json_header,
        )
        print_http_reponse(response)
        try:
            data = json.loads(response.text)
        except:
            data = {}
        return data


    def user_account_info(self, user_id):
        '''
        ref: https://matrix-org.github.io/synapse/latest/admin_api/user_admin_api.html#query-user-account
        '''
        response = requests.get(
            f'''{self.conf['server_url']}/_synapse/admin/v2/users/{user_id}''',
            headers=self.json_header,
        )
        print_http_reponse(response)
        try:
            data = json.loads(response.text)
        except:
            data = {}
        return data


    def create_new_user(self, username=None, displayname=None, password=None):
        '''
        ref: https://matrix-org.github.io/synapse/latest/admin_api/register_api.html
        '''
        response = requests.get(
            f'''{self.conf['server_url']}/_synapse/admin/v1/register''',
            headers=self.json_header,
        )
        print_http_reponse(response)
        try:
            assert username and displayname and password
            data = json.loads(response.text)
            nonce = data['nonce']
            assert data['nonce']
            response = requests.post(
                f'''{self.conf['server_url']}/_synapse/admin/v1/register''',
                json={
                    "nonce": nonce,
                    "username": username,
                    "displayname": displayname,
                    "password": password,
                    "mac": self.generate_mac(nonce, username, password, self.conf['shared_secret'])
                },
                headers=self.json_header
            )
            print_http_reponse(response)
        except Exception as e:
            log.error(f'''ERROR: {e}''')
            data = {}
        return data

    def accept_invitation(self, room_id=None, access_token=None):
        try:
            assert room_id
            if not access_token:
                access_token = self.conf['auth_token']
            response = requests.post(
                f'''{self.conf['server_url']}/_matrix/client/r0/rooms/{room_id}/join''',
                params = {
                    "access_token": access_token,
                }
            )
            print_http_reponse(response)
            return True
        except Exception as e:
            log.error(f'''ERROR accepting invitation: {e}''')
            return False

    def send_message(self, message_body=None, room_id=None, message_body_html=None):
        try:
            assert message_body and room_id
            json = {
                "msgtype": "m.text",
                "body": message_body,
            }
            ## If a formatted message body has been provided, for example if a markdown-formatted
            ## text has been rendered as HTML, include it in the message event
            ## ref: https://spec.matrix.org/v1.2/client-server-api/#mroommessage-msgtypes
            if message_body_html:
                json['format'] = "org.matrix.custom.html"
                json['formatted_body'] = message_body_html
            response = requests.post(
                f'''{self.conf['server_url']}/_matrix/client/r0/rooms/{room_id}/send/m.room.message''',
                params = {
                    "access_token": self.conf['auth_token'],
                },
                json = json
            )
            print_http_reponse(response)
            try:
                assert response.status_code in [200, 204]
                return True
            except:
                log.error(f'''Error sending message to room "{room_id}": [{response.status_code}] {response.text}''')
                return False
        except Exception as e:
            log.error(f'''Error sending message to room "{room_id}": {e}''')
            return False

    def generate_mac(self, nonce, user, password, shared_secret, admin=False, user_type=None):
        mac = hmac.new(
            key=shared_secret.encode('utf8'),
            digestmod=hashlib.sha1,
        )
        mac.update(nonce.encode('utf8'))
        mac.update(b"\x00")
        mac.update(user.encode('utf8'))
        mac.update(b"\x00")
        mac.update(password.encode('utf8'))
        mac.update(b"\x00")
        mac.update(b"admin" if admin else b"notadmin")
        if user_type:
            mac.update(b"\x00")
            mac.update(user_type.encode('utf8'))
        return mac.hexdigest()
