SPT-3G API Server
=======================

Build the SPT API server image for development purposes:

```bash

docker build -t registry.gitlab.com/spt3g/kubernetes/spt-api-server:dev .
```
