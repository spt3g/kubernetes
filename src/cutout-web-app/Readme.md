SPT-3G Web App
=======================

Build the cutout service web app image:

```bash
docker build --build-arg REMOTE_HOSTNAME="spt3g.ncsa.illinois.edu" -t registry.gitlab.com/spt3g/kubernetes/spt-web-app:dev .
```
