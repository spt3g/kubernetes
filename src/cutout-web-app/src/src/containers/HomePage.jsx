import { connect } from "react-redux";
import HomePageComponent from "../components/HomePage";
import { displayError, displayMessage } from "../actions";

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch) => {
	return {
		displayError: (message) => {
			dispatch(displayError(message));
		},
		displayMessage: (message) => {
			dispatch(displayMessage(message));
		},
	};
};

const HomePage = connect(mapStateToProps, mapDispatchToProps)(HomePageComponent);

export default HomePage;
