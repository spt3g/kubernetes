import { connect } from "react-redux";
import JobStatusComponent from "../components/JobStatus";
import { displayError, displayMessage } from "../actions";

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch) => {
	return {
		displayError: (message) => {
			dispatch(displayError(message));
		},
		displayMessage: (message) => {
			dispatch(displayMessage(message));
		},
	};
};

const JobStatus = connect(mapStateToProps, mapDispatchToProps)(JobStatusComponent);

export default JobStatus;
