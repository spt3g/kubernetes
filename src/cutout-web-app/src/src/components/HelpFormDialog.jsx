import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

export default function HelpFormDialog(props) {

	const handleChange = (event) => {
		props.setHelpFormMessage(event.target.value)
	};

	const handleSend = () => {
		props.handleSendClick()
	};

	const handleClose = () => {
		props.closeHelpFormDialog()
	};

	return (
		<div>
			<Dialog
				open={props.open}
				onClose={handleClose}
				scroll={'paper'}
				aria-labelledby="scroll-dialog-title"
				aria-describedby="scroll-dialog-description"
				fullWidth={true}
				maxWidth="md"
			>
				<DialogTitle id="scroll-dialog-title">Help Request</DialogTitle>
				<DialogContent>
					<DialogContentText>
						Send us a note with your questions or feedback.
					</DialogContentText>
				</DialogContent>
				<Box
					component="form"
					sx={{
						'& .MuiTextField-root': { m: 3, width: '90%', 'minWidth': '25ch' },
					}}
					noValidate
					autoComplete="off"
				>
					<div>
						<TextField
							label="Message"
							multiline
							rows={12}
							onChange={handleChange}
						/>
					</div>
				</Box>
				<DialogActions>
					<Button onClick={handleClose}>Cancel</Button>
					<Button onClick={handleSend} variant="contained">Send</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}
