import { React, Component } from "react";
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';

class SelectFiletype extends Component {

	constructor(props) {
		super(props);
		this.state = {
			passthrough: props.filetypes.includes('passthrough'),
			filtered: props.filetypes.includes('filtered'),
		};
		this.state = {
			...this.state,
			inValidSelection: !this.validate(this.state),
		};
		this.handleChange = this.handleChange.bind(this);
		this.validate = this.validate.bind(this);
	}

	handleChange = (event) => {
		this.setState((state, props) => {
			props.setFiletypes({
				...state,
				[event.target.name]: event.target.checked,
			});
			return {
				...state,
				[event.target.name]: event.target.checked,
			};
		}, () => {
			this.setState((state) => {
				return {
					...state,
					inValidSelection: !this.validate(state),
				};
			});
		});
	};

	validate = (state) => {
		return state.passthrough || state.filtered;
	};

	render() {
		return (
			<Box sx={{ display: 'flex' }}>
				<FormControl sx={{ m: 3 }} component="fieldset" variant="standard"
					error={this.state.inValidSelection}>
					<FormLabel component="legend">Select filetypes for Single Observation maps, coadd maps</FormLabel>
					<FormGroup>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.passthrough} onChange={this.handleChange} name="passthrough" />
							}
							label="passthrough"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.filtered} onChange={this.handleChange} name="filtered" />
							}
							label="filtered"
						/>
					</FormGroup>
					<FormHelperText>Select at least one filetype.</FormHelperText>
				</FormControl>
			</Box>
		);
	}
};


export default (SelectFiletype);
