import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function ScrollDialog(props) {

	return (
		<div>
			<Dialog
				open={props.open}
				onClose={props.closeDialog}
				scroll={'paper'}
				aria-labelledby="scroll-dialog-title"
				aria-describedby="scroll-dialog-description"
				fullWidth={true}
				maxWidth="lg"
			>
				<DialogTitle id="scroll-dialog-title">Job ID: <code>{props.jobId}</code></DialogTitle>
				<DialogContent dividers={true}>
					<DialogContentText
						id="scroll-dialog-description"
						// ref={descriptionElementRef}
						tabIndex={-1}
					>
						<pre sx={{whiteSpace: "pre-wrap"}}>{props.bodyText}</pre>
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={props.closeDialog}>Close</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}
