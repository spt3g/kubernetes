import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

export default function InfoMessage(props) {
	
	return (
		<Box sx={{ width: '80vw', position: "fixed", opacity: "1.0",
			zIndex: (theme) => theme.zIndex.drawer + 1,
		}}>
			<Snackbar open={props.messageOpen} autoHideDuration={6000} onClose={props.closeInfoMessage}>
				<Alert onClose={props.closeInfoMessage} severity={props.severity} variant="filled" sx={{ width: '100%' }}>
				{props.message}
				</Alert>
			</Snackbar>
		</Box>
	);
}

