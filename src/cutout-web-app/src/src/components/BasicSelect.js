import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function BasicSelect(props) {
  const [coaddId, setCoaddId] = React.useState(props.value);

  const handleChange = (event) => {
    setCoaddId(event.target.value);
  };

  const menuItems = props.coaddIds.map((coaddId, index, array) => {
    return (
      <MenuItem value={coaddId} key={index}>{coaddId}</MenuItem>
    )
  });

  return (
    <Box>
      <FormControl sx={{ width: '80%', minWidth: '12rem' }}>
        <InputLabel id="coaddId-select-label">Yearly Coadd</InputLabel>
        <Select
          labelId="coaddId-select-label"
          id="coaddId-select"
          value={coaddId}
          label="Yearly Coadd"
          onChange={handleChange}
        >
          {menuItems}
        </Select>
      </FormControl>
    </Box>
  );
}
