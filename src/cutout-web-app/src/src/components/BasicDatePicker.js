import * as React from 'react';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';


export default function BasicDatePicker(props) {
	const [value, setValue] = React.useState(null);
	return (
		<LocalizationProvider dateAdapter={AdapterDateFns}>
			<DatePicker
				label={props.whichDate}
				value={value || props.initDate }
				onChange={(newValue) => {
					setValue(newValue);
					props.setDate(newValue);
				}}
				renderInput={(params) => <TextField {...params} sx={{ width: '80%', minWidth: '12rem'}}/>}
			/>
		</LocalizationProvider>
	);
}
