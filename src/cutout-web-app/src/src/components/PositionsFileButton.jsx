import * as React from 'react';
import Box from '@mui/material/Box';
import {
	Button,
} from '@mui/material';

export default function PositionsFileButton(props) {
	const [, setValue] = React.useState(props.value);

	/* read csv, update positions */
	const handleCsvUpload = async (e) => {
		let csv_file = e.target.files[0];
		var csv_text = await csv_file.text();
		setValue(csv_text);
		props.setPositions(csv_text)
	};

	return (
		<Box
			component="form"
			noValidate
			autoComplete="off"
		>
			<div>
				<Button variant="contained" style={{
					width: '10rem',
				}}>
					<span style={{
						overflowX: "auto",
						overflowWrap: "break-word",
					}}>Upload CSV file</span>
					<input type="file" accept=".csv, .CSV" onChange={handleCsvUpload} style={{
						position: 'absolute',
						opacity: 0,
						top: '0px',
						left: '0px',
						width: '100%',
						height: '100%',
					}}
				/>
				</Button>
			</div>
		</Box>
	);
} 
