import { Component } from "react";
import {
	Container,
	// Button,
} from '@mui/material';
import { withStyles } from '@mui/styles';
// import JobTable from './JobTable';
import config from "../app.config";
import Cookies from "universal-cookie";
import { DataGrid } from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import DescriptionIcon from '@mui/icons-material/Description';
import InfoIcon from '@mui/icons-material/Info';
import {
	GridActionsCellItem,
} from '@mui/x-data-grid';
import ConfirmDeleteDialog from './ConfirmDeleteDialog';
import ScrollDialog from "./ScrollDialog.jsx";
// import yaml from 'js-yaml';

const cookies = new Cookies();

const styles = theme => ({
	root: {
		color: theme.palette.primary,
		position: "relative",
		display: "flex",
		alignItems: "center",
		[theme.breakpoints.up("sm")]: {
			minHeight: 400,
			maxHeight: 1300,
		},
	},
	container: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(10),
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		maxHeight: "90%",
	},
	backdrop: {
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		opacity: 0.7,
		zIndex: -1,
	},
	background: {
		backgroundColor: "#ffffff",
		backgroundPosition: "center",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		backgroundSize: "cover",
		zIndex: -2,
	},
	releaseChip: {
		minWidth: 200,
		padding: 25,
		margin: theme.spacing(2),
		color: "#ffffff",
		fontWeight: "bold",
	},
	h1: {
		fontWeight: "bold",
		color: theme.palette.primary
	},
	h5: {
		marginBottom: theme.spacing(3),
		marginTop: theme.spacing(3),
	},
	more: {
		textAlign: "center",
		marginTop: theme.spacing(2),
	},
	versionSection: {
		margin: theme.spacing(0.5)
	},
	versionLine: {
		margin: theme.spacing(1)
	},
	versioning: {
		margin: theme.spacing(0.5),
		fontSize: 14,
		// display: "inline",
		fontWeight: "bold"
	},
	sectionDark: {
		display: "flex",
		overflow: "hidden",
		backgroundColor: theme.palette.fourth.main,
	},
	sectionLight: {
		display: "flex",
		overflow: "hidden"
	},
	footer: {
		overflow: "hidden",
		backgroundColor: "#ffffff"
	},
	link: {
		color: theme.palette.secondary.main,
		textDecoration: "none",
		fontWeight: "bold"
	},
	sectionContainers: {
		marginTop: theme.spacing(15),
		marginBottom: theme.spacing(30),
		display: "flex",
		position: "relative",
	},
	item: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		padding: theme.spacing(0, 3),
	},
	image: {
		height: 100,
	},
	title: {
		marginTop: theme.spacing(3),
		minHeight: theme.spacing(10),
	},
	content: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		textAlign: "center",
	},
	footerContainer: {
		marginTop: theme.spacing(4),
		marginBottom: theme.spacing(4),
		display: "flex",
	},
	iconsWrapper: {
		height: 80,
	},
	icons: {
		display: "flex",
	},
	icon: {
		height: 80,
		borderRadius: 5,
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		marginRight: theme.spacing(1)
	},
	list: {
		margin: 0,
		listStyle: "none",
		paddingLeft: 0,
	},
	listItem: {
		paddingTop: theme.spacing(0.5),
		paddingBottom: theme.spacing(0.5),
	},
});

class JobStatus extends Component {

	constructor(props) {
		super(props);
		this.state = {
			jobList: [],
			confirmDeleteDialogOpen: false,
			jobIdToDelete: '',
			logText: '',
			logJobId: '',
			openLogView: false,
			nextStatusUpdate: 30,
			sortModel: [{
				field: 'jobStatus',
				sort: 'desc',
			},
			{
				field: 'startTime',
				sort: 'desc',
			}]
			// nextStatusUpdate: new Date((new Date).valueOf() + 30),
		};
		this.listJobs = this.listJobs.bind(this);
		this.handleResponse = this.handleResponse.bind(this);
		this.handleDeleteClick = this.handleDeleteClick.bind(this);
		this.requestDeleteJob = this.requestDeleteJob.bind(this);
		this.confirmDeleteClick = this.confirmDeleteClick.bind(this);
		this.closeConfirmDeleteDialog = this.closeConfirmDeleteDialog.bind(this);
		this.closeLogView = this.closeLogView.bind(this);
		this.showLog = this.showLog.bind(this);
		this.setSortModel = this.setSortModel.bind(this);
	}

	componentDidMount() {
		this.listJobs();
		this.jobStatusPoller = setInterval(() => {
			this.listJobs();
			this.setState({
				nextStatusUpdate: 30,
				// nextStatusUpdate: new Date((new Date).valueOf() + 30),
			})
		}, 30000);
		this.jobStatusPollerTimer = setInterval(() => {
			this.setState({
				nextStatusUpdate: this.state.nextStatusUpdate - 1,
			})
		}, 1000);
	}

	componentWillUnmount() {
		clearInterval(this.jobStatusPoller);
		clearInterval(this.jobStatusPollerTimer);
	}

	async handleResponse(response) {
		if (response.status >= 200 && response.status <= 299) {
			let jobRecords = await response.json();
			// console.log(JSON.stringify(jobRecords, null, 2));
			let jobList = jobRecords.map((jobInfo, index, array) => {
				return {
					id: jobInfo['job_id'],
					// runId: jobInfo['run_id'],
					jobStatus: jobInfo['phase'],
					startTime: jobInfo['time_start'],
					endTime: jobInfo['time_end'],
					queuePosition: jobInfo['queue_position'],
				}
			});
			// console.log(jobList);
			this.setState({
				jobList: jobList,
			})
		} else {
			this.props.displayError(`Error updating status: [${response.status}] ${response.statusText}`);
			console.log(JSON.stringify(response.statusText, null, 2));
		}
	}

	async requestDeleteJob(jobId) {
		const endpoint = `https://${config.hostname}${config.jobSubmitUrl}/${jobId}`;
		let response = await fetch(endpoint, {
			method: "DELETE",
			headers: {
				"Authorization": cookies.get("Authorization")
			},
		});
		if (response.status >= 200 && response.status <= 299) {
			// console.log(`Job ${jobId} deleted successfully.`);
			this.listJobs();
		} else {
			this.props.displayError(`Error deleting job ${jobId}: [${response.status}] ${response.statusText}`);
			console.log(JSON.stringify(response.statusText, null, 2));
		}
	}

	async listJobs() {
		const endpoint = `https://${config.hostname}${config.jobSubmitUrl}`;
		let jobRequest = await fetch(endpoint, {
			method: "GET",
			headers: {
				"Authorization": cookies.get("Authorization")
			},
		});
		this.handleResponse(jobRequest)
	}

	confirmDeleteClick(event, jobId) {
		this.setState({
			confirmDeleteDialogOpen: true,
			jobIdToDelete: jobId,
		})
	};

	handleDeleteClick(jobId) {
		// console.log(`Deleting row ${jobId}...`)
		this.requestDeleteJob(jobId);
		this.setState({
			confirmDeleteDialogOpen: false,
			jobIdToDelete: '',
		})
	};

	closeConfirmDeleteDialog() {
		this.setState({
			confirmDeleteDialogOpen: false,
		})
	};

	closeLogView() {
		this.setState({
			openLogView: false,
			logText: '',
			logJobId: '',
		})
	};

	setSortModel(model) {
		this.setState({
			sortModel: model,
		})
	};

	async showLog(id) {
		const endpoint = `https://${config.hostname}${config.jobFilesBaseUrl}/${id}/out/job.log`;
		let response = await fetch(endpoint, {
			method: "GET",
		});
		if (response.status >= 200 && response.status <= 299) {
			let jobLogText = await response.text();
			this.setState({
				openLogView: true,
				logText: jobLogText,
				logJobId: id,
			})
		} else if (response.status === 404) {
			this.props.displayError(`job.log file not found`);
		} else {
			this.props.displayError(`${response.statusText}: ${await response.text()}`);
		}
	}

	async showManifest(id) {
		let jobManifestYaml = '';
		// let manifest = {};
		try {
			const endpoint = `https://${config.hostname}${config.jobFilesBaseUrl}/${id}/out/manifest.json`;
			let response = await fetch(endpoint, {
				method: "GET",
			});
			if (response.status >= 200 && response.status <= 299) {
				jobManifestYaml = await response.text();
				// manifest = yaml.load(jobManifestYaml);
				// console.log(manifest);
				this.setState({
					openLogView: true,
					logText: jobManifestYaml,
					// logText: manifest['files'].join(','),
					logJobId: id,
				})
			} else if (response.status === 404) {
				this.props.displayError(`manifest.json file not found`);
			} else {
				this.props.displayError(`${response.statusText}: ${await response.text()}`);
			}
		} catch (e) {
			console.log(e);
		}
	}

	columns = [
		{
			field: 'id',
			headerName: 'ID',
			description: 'Unique job ID',
			sortable: false,
			flex: 1,
			minWidth: 100
		},
		// {
		// 	field: 'runId',
		// 	headerName: 'Label',
		// 	description: 'Job Label',
		// 	sortable: true,
		// 	flex: 1,
		// 	minWidth: 100
		// },
		{
			field: 'queuePosition',
			headerName: 'Queue',
			description: 'Initial position in queue',
			type: 'integer',
			sortable: false,
			flex: 1,
			minWidth: 10,
		},
		{
			field: 'jobStatus',
			headerName: 'Status',
			description: 'Status of job',
			sortable: true,
			flex: 1,
			minWidth: 100
		},
		{
			field: 'startTime',
			headerName: 'Start Time',
			description: 'Job start time (UTC)',
			type: 'dateTime',
			sortable: true,
			flex: 1,
			minWidth: 100,
		},
		{
			field: 'endTime',
			headerName: 'End Time',
			description: 'Job end time (UTC)',
			type: 'dateTime',
			sortable: true,
			flex: 1,
			minWidth: 100,
		},
		{
			field: 'outputs',
			type: 'actions',
			headerName: 'Outputs',
			width: 200,
			getActions: ({ id }) => {
				let actionCellItems = []
				actionCellItems.push(
					<GridActionsCellItem
						icon={<InfoIcon />}
						label="View job info"
						onClick={() => { this.showManifest(id) }}
						color="inherit"
						sx={{
							display: () => {
								var jobStatus = '';
								this.state.jobList.filter((obj) => {
									if (obj['id'] === id) {
										jobStatus = obj['jobStatus'];
									}
									return obj;
								});
								if (['pending', 'queued'].includes(jobStatus)) {
									return 'none';
								} else {
									return 'block';
								}
							}
						}}
					/>
				);
				actionCellItems.push(
					<GridActionsCellItem
						icon={<DescriptionIcon />}
						label="View job log"
						onClick={() => { this.showLog(id) }}
						color="inherit"
						sx={{
							display: () => {
								var jobStatus = '';
								this.state.jobList.filter((obj) => {
									if (obj['id'] === id) {
										jobStatus = obj['jobStatus'];
									}
									return obj;
								});
								if (['pending', 'queued'].includes(jobStatus)) {
									return 'none';
								} else {
									return 'block';
								}
							}
						}}
					/>
				);
				actionCellItems.push(
					<GridActionsCellItem
						icon={<FileDownloadIcon />}
						label="Download job files"
						onClick={() => {window.open(`https://${config.hostname}${config.jobFilesBaseUrl}/${id}/out`, '_blank');}}
						color="inherit"
						sx={{
							display: () => {
								var jobStatus = '';
								this.state.jobList.filter((obj) => {
									if (obj['id'] === id) {
										jobStatus = obj['jobStatus'];
									}
									return obj;
								});
								if (['pending', 'queued'].includes(jobStatus)) {
									return 'none';
								} else {
									return 'block';
								}
							}
						}}
					/>
				);
				return actionCellItems;
			},
		},
		{
			field: 'actions',
			type: 'actions',
			headerName: 'Actions',
			width: 100,
			getActions: ({ id }) => {

				return [
					<GridActionsCellItem
						icon={<DeleteIcon />}
						label="Delete"
						onClick={(event) => {this.confirmDeleteClick(event, id)}}
						color="inherit"
					/>,
				];
			},
		},
	]

	render() {
		const { classes } = this.props;
		return (
			<Container className={classes.container}>
				{/* <JobTable jobs={this.state.jobList} /> */}
				<div style={{ height: '80vh', width: '100%' }}>
					<DataGrid
						rows={this.state.jobList}
						columns={this.columns}
						// autoHeight
						// pageSize={5}
						// autoPageSize={true}
						rowsPerPageOptions={[100, 20, 5]}
						// checkboxSelection
						onSortModelChange={(model) => this.setSortModel(model)}
						sortModel={this.state.sortModel}
					/>
				<div><p>Next auto-update: {this.state.nextStatusUpdate} sec</p></div>
				</div>
				{/* <Button variant="contained" onClick={this.listJobs}>Update Status</Button> */}
				{/* <div><p>Next auto-update: {this.state.nextStatusUpdate.toLocaleTimeString()} sec</p></div> */}
				<ConfirmDeleteDialog
					open={this.state.confirmDeleteDialogOpen}
					jobId={this.state.jobIdToDelete}
					deleteJob={this.handleDeleteClick}
					closeConfirmDeleteDialog={this.closeConfirmDeleteDialog}
				/>
				<ScrollDialog jobId={this.state.logJobId} bodyText={this.state.logText} open={this.state.openLogView} closeDialog={this.closeLogView} />
			</Container>
		);
	}
}

export default withStyles(styles)(JobStatus);
