import { React, Component } from "react";
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';

class SelectCoaddId extends Component {

	constructor(props) {
		super(props);
		this.state = {
			option1: props.coaddIds.includes('yearly_winter_2019-2023'),
			option6: props.coaddIds.includes('yearly_collate_widef'),
			option7: props.coaddIds.includes('yearly_collate_wideg'),
			option8: props.coaddIds.includes('yearly_collate_widei'),
			option9: props.coaddIds.includes('yearly_summer_a_2019-2022'),
			option10: props.coaddIds.includes('yearly_summer_b_2019-2022'),
			option11: props.coaddIds.includes('yearly_summer_c_2019-2022'),
		};
		this.state = {
			...this.state,
			inValidSelection: !this.validate(this.state),
		};
		this.handleChange = this.handleChange.bind(this);
		this.validate = this.validate.bind(this);
	}

	handleChange = (event) => {
		this.setState((state, props) => {
			props.setCoaddIds({
				...state,
				[event.target.name]: event.target.checked,
			});
			return {
				...state,
				[event.target.name]: event.target.checked,
			};
		}, () => {
			this.setState((state) => {
				return {
					...state,
					inValidSelection: !this.validate(state),
				};
			});
		});
	};

	validate = (state) => {
		return state.option1 || state.option6 || state.option7 || state.option8 || state.option9 || state.option10 || state.option11;
	};

	render() {
		return (
			<Box sx={{ display: 'flex' }}>
				<FormControl sx={{ m: 3 }} component="fieldset" variant="standard"
					error={this.state.inValidSelection}>
					<FormLabel component="legend">Select Coadd ID for coadds/yearly only</FormLabel>
					<FormGroup>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option1} onChange={this.handleChange} name="option1" />
							}
							label="winter_2019-2023"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option6} onChange={this.handleChange} name="option6" />
							}
							label="collate_widef"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option7} onChange={this.handleChange} name="option7" />
							}
							label="collate_wideg"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option8} onChange={this.handleChange} name="option8" />
							}
							label="collate_widei"
						/>

						<FormControlLabel
							control={
								<Checkbox checked={this.state.option9} onChange={this.handleChange} name="option9" />
							}
							label="summer_a_2019-2022"
						/>

						<FormControlLabel
							control={
								<Checkbox checked={this.state.option10} onChange={this.handleChange} name="option10" />
							}
							label="summer_b_2019-2022"
						/>

						<FormControlLabel
							control={
								<Checkbox checked={this.state.option11} onChange={this.handleChange} name="option11" />
							}
							label="summer_c_2019-2022"
						/>


					</FormGroup>
					<FormHelperText></FormHelperText>
				</FormControl>
			</Box>
		);
	}
};


export default (SelectCoaddId);
