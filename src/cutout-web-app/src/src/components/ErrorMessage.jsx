import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';

export default function ErrorMessage(props) {
	return (
		<Box sx={{ width: '80vw', position: "fixed", opacity: "1.0",
				zIndex: (theme) => theme.zIndex.drawer + 1,
			}}>
			<Collapse in={props.messageOpen}>
				<Alert severity="error" variant="filled" action={
					<IconButton
						aria-label="close"
						color="inherit"
						size="small"
						onClick={props.closeErrorMessage}
					>
						<CloseIcon fontSize="inherit" />
					</IconButton>
				}
				>{props.error}</Alert>
			</Collapse>
		</Box>
	);
}

