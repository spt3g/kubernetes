import { Component } from "react";
import {
	AppBar,
	Button,
	Divider,
	Link,
	Menu,
	MenuItem,
	Box,
	Toolbar,
	Typography,
	Avatar,
	IconButton,
	Drawer,
} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import ContentCutIcon from '@mui/icons-material/ContentCut';
import SpeedIcon from '@mui/icons-material/Speed';
import HomeIcon from '@mui/icons-material/Home';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import CssBaseline from '@mui/material/CssBaseline';
import { browserHistory } from "react-router";

// ref: https://mui.com/styles/api/#themeprovider
import { createTheme, ThemeProvider } from '@mui/material/styles';
// ref: https://mui.com/styles/api/#withstyles-styles-options-higher-order-component
import { withStyles } from '@mui/styles';
import config from "../app.config";
// import ErrorMessage from "./ErrorMessage";
import InfoMessage from "./InfoMessage";

const theme = createTheme({
	palette: {
		primary: {
			// main: "#18381b"
			main: "#5daefd"
		},
		secondary: {
			main: "#e8a114"
		},
		third: {
			main: "#333333"
		},
		fourth: {
			main: "#eeeeee"
		}
	},
	typography: {
		h1: {
			fontFamily: "'Roboto Condensed',sans-serif"
		},
		h2: {
			fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif"
		},
		h3: {
			fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif"
		},
		h4: {
			fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif"
		},
		h5: {
			fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
			fontSize: { xs: '1rem', sm: '2rem' },
		},
		h6: {
			fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif"
		},
		body1: {
			fontFamily: "'Work Sans',sans-serif",
			fontSize: "14px"
		},
		body2: {
			fontFamily: "'Work Sans',sans-serif",
			fontSize: "12px"
		}
	},
});

const styles = {
	menuCustomWidth: {
		"& li": {
			width: "200px"
		}
	},
	appBar: {
		width: "100%",
		transition: theme.transitions.create(["margin", "width"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	avatarImg: {
		width: "32px",
		height: "32px",
		borderRadius: "50%",
		// border:"solid 2px #FFFFFF"
	},
	toolBar: {
		minHeight: "48px",
		display: "flex",
		justifyContent: "flex-start",
	},
	smallButton: {
		padding: "6px"
	},
	denseStyle: {
		minHeight: "10px",
		lineHeight: "30px",
		fontSize: "12px",
	},
	status: {
		padding: "6px 16px"
	},
	fontLight: {
		fontSize: "12px",
		color: "#333333",
		fontWeight: 100,
		fontFamily: theme.typography.body1.fontFamily,
	},
	fontBold: {
		fontSize: "12px",
		color: "#000000",
		fontWeight: 600,
		fontFamily: theme.typography.body1.fontFamily,
	},
	toolBarItem: {
		margin: "auto 20px",
		cursor: "pointer"
	},
	customProgressBar: {
		borderRadius: 5,
		height: 5,
	},
	bannerText: {
		"&:hover": {
			cursor: "pointer"
		},
	 }
};

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			origin: props.location.pathname,
			authError: false,
			profileMenuOpen: false,
			anchorEl: null,
			errorMessage: "",
			messageOpen: false,
			mobileOpen: false,
		};
		this.logout = this.logout.bind(this);
		this.handleProfileMenuOpen = this.handleProfileMenuOpen.bind(this);
		this.handleProfileMenuClose = this.handleProfileMenuClose.bind(this);
		this.fetchAuthToken = this.fetchAuthToken.bind(this);
		this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
		this.closeDrawer = this.closeDrawer.bind(this);
	}

	componentDidMount() {
		if (this.props.Authorization !== "" && this.props.Authorization !== undefined && this.props.userInfo !== "" && this.props.userInfo !== undefined) {
			console.log("Already logged in.")
		}
		else {
			this.fetchAuthToken();
		}
	}

	async fetchAuthToken() {
		await this.props.fetchAuthToken();
		if (this.props.loginError) {
			let errorMessage = `Login error: ${this.props.loginResponseText}`;
			this.setState({
				errorMessage: errorMessage
			});
			this.props.displayError(errorMessage)
		}
	}


	handleProfileMenuOpen(event) {
		this.setState({
			profileMenuOpen: true,
			anchorEl: event.currentTarget
		});
	}

	handleProfileMenuClose = (event) => {
		this.setState({
			profileMenuOpen: false,
		});
	}

	handleProfileMenuClose() {
		this.setState({
			profileMenuOpen: false,
		});
	}

	handleDrawerToggle = () => {
		this.setState({
			mobileOpen: !this.state.mobileOpen,
		});
	};

	closeDrawer = () => {
		this.setState({
			mobileOpen: false,
		});
	};

	logout() {
		this.props.logout();
		window.location.assign(config.logoutUrl);
		this.setState({
			profileMenuOpen: false,
		});
	}

	render() {
		const { classes } = this.props;
		const { homepage, cutout, status } = this.props
		
		let username;

		let contents = <Button color="inherit" href={config.baseUrl} className={classes.smallButton}>Login</Button>;
		let profileMenu = <></>;
		if (this.props.Authorization !== "" && this.props.Authorization !== undefined && this.props.userInfo !== "" && this.props.userInfo !== undefined) {
			const userInfo = this.props.userInfo;
			// console.log(userInfo)
			if (userInfo["preferred_username"] !== undefined) {
				username = userInfo["preferred_username"];
			} else {
				username = config.testUserInfo["preferred_username"];
			}
			const initials = `${userInfo["given_name"].charAt(0)}${userInfo["family_name"].charAt(0)}`;

			contents = (
				<Box className={classes.status}>
					<IconButton color="inherit" className={classes.smallButton} onClick={this.handleProfileMenuOpen}>
						<Avatar fontSize="small" sx={{ backgroundColor: "black" }}>{initials}</Avatar>
					</IconButton>
				</Box>
			);

			profileMenu = (
				<Menu
					anchorEl={this.state.anchorEl}
					anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
					keepMounted
					transformOrigin={{ vertical: "top", horizontal: "center" }}
					open={this.state.profileMenuOpen}
					onClose={this.handleProfileMenuClose}
					// getContentAnchorEl={null}
					className={classes.menuCustomWidth}
				>
					<Box className={classes.status}>
						<Typography className={classes.fontLight}>Signed in as</Typography>
						<Typography className={classes.fontBold}>{username}</Typography>
					</Box>
					<Divider orientation="horizontal" />
					<Link href={config.accountUrl} target="_blank" style={{ textDecoration: "none" }}>
						<MenuItem className={classes.denseStyle}>
							Account
						</MenuItem>
					</Link>
					<Divider orientation="horizontal" />
					<Link style={{ textDecoration: "none" }} onClick={() => {
						this.handleProfileMenuClose();
						this.logout();
					}}>
						<MenuItem className={classes.denseStyle}>
							Log Out
						</MenuItem>
					</Link>
				</Menu>
			);
		}

		// const container = window !== undefined ? () => window().document.body : undefined;
		// console.log(`container: ${container}`)
		const drawerWidth = 240;

		const drawer = (
			<div>
				<Toolbar />
				<Divider />
				<List sx={{ paddingTop: '2rem' }}>
					<Link style={{ color: "#000000", textDecoration: "none" }}
						onClick={(event) => {
							// event.stopPropagation();
							browserHistory.push(`${config.baseUrl}`);
							this.closeDrawer();
						}}
					>
						<ListItem button key="Home">
							<ListItemIcon>
								<HomeIcon />
							</ListItemIcon>
							<ListItemText primary="Home" />
						</ListItem>
					</Link>

					<Link style={{ color: "#000000", textDecoration: "none" }}
						onClick={(event) => {
							// event.stopPropagation();
							browserHistory.push(`${config.baseUrl}/cutout`);
							this.closeDrawer();
						}}
					>
						<ListItem button key="Cutout">
							<ListItemIcon>
								<ContentCutIcon />
							</ListItemIcon>
							<ListItemText primary="Cutout" />
						</ListItem>
					</Link>

					<Link style={{ color: "#000000", textDecoration: "none" }}
						onClick={(event) => {
							// event.stopPropagation();
							browserHistory.push(`${config.baseUrl}/status`);
							this.closeDrawer();
						}}
					>
						<ListItem button key="Status">
							<ListItemIcon>
								<SpeedIcon />
							</ListItemIcon>
							<ListItemText primary="Status" />
						</ListItem>
					</Link>
				</List>
			</div>
		);

		return (
			<ThemeProvider theme={theme}>
				<Box sx={{ display: 'flex' }}>
					<CssBaseline />

					<AppBar position="fixed"
						sx={{
							zIndex: (theme) => theme.zIndex.drawer + 1,
							// width: { sm: `calc(100% - ${drawerWidth}px)` },
							// ml: { sm: `${drawerWidth}px` },
						}}
						className={classes.appBar}>
						<Toolbar className={classes.toolBar}>
							<IconButton
								color="inherit"
								aria-label="open drawer"
								edge="start"
								onClick={this.handleDrawerToggle}
								sx={{ mr: 2, display: { sm: 'none' } }}
							>
								<MenuIcon />
							</IconButton>
								<Link className={classes.bannerText} style={{ color: "#ffffff", textDecoration: "none" }}
									sx={{
										fontSize: { xs: '1rem', sm: '1.5rem' },
									}}
									onClick={(event) => {
										// event.stopPropagation();
										browserHistory.push(`${config.baseUrl}`);
									}}
								>
										<Typography variant="h5">
									SPT-3G // Cutout Service
									{this.props.location.pathname.replace(/\/$/, '') === `${config.baseUrl}/cutout` ? ' / Request Cutout' : ''}
									{this.props.location.pathname.replace(/\/$/, '') === `${config.baseUrl}/status` ? ' / Job Status' : ''}
							</Typography>
								</Link>
							<Typography variant="body1" style={{ flex: 1 }} />
							{contents}
							{profileMenu}
						</Toolbar>
					</AppBar>

					<Box
						component="nav"
						sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
						aria-label="apps"
					>
						<Drawer
							container={document.querySelector("body")}
							variant="temporary"
							open={this.state.mobileOpen}
							onClose={this.handleDrawerToggle}
							ModalProps={{
								keepMounted: true, // Better open performance on mobile.
							}}
							sx={{
								display: { xs: 'block', sm: 'none' },
								'& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
							}}
							style={{ position: "absolute" }}
						>
							{drawer}
						</Drawer>
						<Drawer
							variant="permanent"
							anchor="left"
							sx={{
								display: { xs: 'none', sm: 'block' },
								'& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
							}}
							open
						>
							{drawer}
						</Drawer>
					</Box>

					<Box
						component="main"
						sx={{ flexGrow: 1, p: 3, width: { xs: '100%', sm: `calc(100% - ${drawerWidth}px)` } }}
					>
						<Toolbar />
							<InfoMessage severity="success" message={this.props.infoMessageText} messageOpen={this.props.infoMessageDisplay} closeInfoMessage={this.props.clearMessage} />
							<InfoMessage severity="error" message={this.props.errorMessageText} messageOpen={this.props.errorMessageDisplay} closeInfoMessage={this.props.clearError} />
						<div className={classes.appBar}>
							{homepage}
							{cutout}
							{status}
						</div>
					</Box>
				</Box>
			</ThemeProvider>
		);
	}

}

export default withStyles(styles)(App);
