type UserState = {
	Authorization: string,
	userInfo: string,
	loginError: boolean,
	loginResponseText: string,
}

type UserAction = {
	type: String,
	Authorization: String,
	userInfo: String,
	loginError: boolean,
	loginResponseText: string,
}

const defaultState: UserState = {
	Authorization: "",
	userInfo: "",
	loginError: false,
	loginResponseText: "",
};

const user = (state: UserState = defaultState, action: UserAction) => {
	switch (action.type) {
		case 'login/succeeded':
			return Object.assign({}, state, { Authorization: action.Authorization, userInfo: action.userInfo, loginError: false, loginResponseText: "" });
		case 'login/failed':
			return Object.assign({}, state, { Authorization: "", userInfo: "", loginError: true, loginResponseText: action.loginResponseText });
		case 'logout':
			return Object.assign({}, state, { Authorization: "", userInfo: "", loginError: false, loginResponseText: "" });
		default:
			return state;
	}
};

export default user;
