type ErrorMessageState = {
	display: Boolean,
	message: String,
}

type ErrorMessageAction = {
	type: String,
	message: String,
}

const defaultState: ErrorMessageState = {
	display: false,
	message: "",
};

const errorMessage = (state: ErrorMessageState = defaultState, action: ErrorMessageAction) => {
	switch (action.type) {
		case 'errorMessage/display':
			return Object.assign({}, state, { display: true, message: action.message });
		case 'errorMessage/clear':
			return Object.assign({}, state, { display: false, message: "" });
		default:
			return state;
	}
};

export default errorMessage;
