import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import user from "./user";
import errorMessage from "./errorMessage";
import infoMessage from "./infoMessage";

const rootReducer = combineReducers({
	routing: routerReducer,
	user: user,
	errorMessage: errorMessage,
	infoMessage: infoMessage,
});

export default rootReducer;
