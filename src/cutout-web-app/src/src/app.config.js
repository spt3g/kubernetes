let config = {};
let hostname = process.env.REACT_APP_REMOTE_HOSTNAME || "localhost";

config["hostname"] = hostname;
config["baseUrl"] = '/app';
config["baseApiUrl"] = '/api/v1';
config["accountUrl"] = '/account';
config["loginUrl"] = '/account/login';
config["logoutUrl"] = '/account/logout';
config["tokenUrl"] = '/account/token/create';
config["tokenRefreshPath"] = '/api/v1/account/token/refresh';
config["sendHelpFormUrl"] = '/api/v1/help';
config["jobSubmitUrl"] = '/api/v1/uws/job';
config["jobFilesBaseUrl"] = '/files/jobs';
config["profileApiUrl"] = '/api/v1/profile';
config["testUserInfo"] = {};
config["testUserInfo"]['email'] = "vera.rubin@example.com";
config["testUserInfo"]['preferred_username'] = "fridakahlo";
config["testUserInfo"]['given_name'] = "Frida";
config["testUserInfo"]['family_name'] = "Kahlo";
config["testUserInfo"]['name'] = "Frida Kahlo";
config["testUserInfo"]['groups'] = ["Collaborator"];
config["cutouts"] = {};
config["cutouts"]["defaultCoaddIds"] = ["yearly_winter_2019-2023"]
config["cutouts"]["defaultBands"] = ["150GHz"]
config["cutouts"]["defaultFiletypes"] = ["passthrough"]
config["cutouts"]["defaultPositions"] = `RA,DEC,XSIZE,YSIZE\n358.3406816, -58.9660379, 10, 10`

export default config;
