##
## DecentCI Infrastructure
##
infrastructure:
  argocd:
    enabled: true
    project: spt3g
    sidecar:
      enabled: true
      project: spt3g
      repoURL: https://gitlab.com/spt3g/kubernetes.git
      path: "apps/decentci/sidecars/argocd-sidecar"
      targetRevision: "main"
    values:
      argocd:
        global:
          domain: "spt3g.ncsa.illinois.edu"
        configs:
          cm:
            url: https://spt3g.ncsa.illinois.edu/argo-cd
            dex.config: |
              connectors:
              - id: gitlab-spt3g
                type: gitlab
                name: SPT-3G on GitLab
                config:
                  groups:
                  - "spt3g"
                  clientID: $dex.gitlab.spt3g.clientId
                  clientSecret: $dex.gitlab.spt3g.clientSecret
  sealedsecrets:
    enabled: true
    project: spt3g
  certmanager:
    enabled: true
    project: spt3g
    values:
      clusterIssuer:
        email: "manninga@illinois.edu"
  traefikcertmanager:
    enabled: true
    project: spt3g
    values:
      image:
        repo: hub.ncsa.illinois.edu/nsf-muses/traefik-certmanager
      options:
        support_legacy_crds: false
  traefik:
    enabled: true
    project: spt3g
    values:
      service:
        ## List of external IP addresses
        externalIPs:
        - 141.142.221.207
        spec:
          loadBalancerIP: "192.168.3.65"
      additionalArguments:
      - --providers.kubernetesingress.ingressendpoint.ip=141.142.221.207
  metallb:
    enabled: true
    project: spt3g
    values:
      IPAddressPool:
        addresses:
          - 192.168.3.65/32
  longhorn:
    enabled: true
    project: spt3g
    values:
      defaultSettings:
        backupTarget: "nfs://taiga-nfs.ncsa.illinois.edu:/taiga/ncsa/radiant/bbfl/k8s-cluster-spt3g/backups/longhorn"
  nfsprovisioner:
    enabled: true
    project: spt3g
    values:
      storageClass:
        name: nfs-taiga
      nfs:
        server: "taiga-nfs.ncsa.illinois.edu"
        path: "/taiga/ncsa/radiant/bbfl/k8s-cluster-spt3g/pv/nfs-taiga"
  keel:
    enabled: true
    project: spt3g
##
## DecentCI Services
##
services:
  backups:
    enabled: true
    project: spt3g
    values:
      server:
        monitor:
          annotations: false
      restic:
        server:
          enabled: false
          ingress:
            enabled: false
            hostname: "restic-server.spt3g.ncsa.illinois.edu"
  hedgedoc:
    enabled: false
    project: spt3g
    sidecar:
      enabled: false
      project: spt3g
      repoURL: https://gitlab.com/spt3g/kubernetes.git
      path: "apps/decentci/sidecars/hedgedoc-sidecar"
      targetRevision: "main"
    values:
      ingress:
        hostname: "docs.spt3g.ncsa.illinois.edu"
      server:
        config:
          oauth:
            enabled: true
            providerName: "SSO"
            urls:
              base: "https://keycloak.spt3g.ncsa.illinois.edu/realms/spt3g/protocol/openid-connect"
              token: "https://keycloak.spt3g.ncsa.illinois.edu/realms/spt3g/protocol/openid-connect/token"
              authorize: "https://keycloak.spt3g.ncsa.illinois.edu/realms/spt3g/protocol/openid-connect/auth"
              userinfo: "https://keycloak.spt3g.ncsa.illinois.edu/realms/spt3g/protocol/openid-connect/userinfo"
            accessRole: "/spt-default"
      postgresqlha:
        postgresql:
          replicaCount: 2
  keycloak:
    enabled: true
    project: spt3g
    sidecar:
      enabled: true
      project: spt3g
      repoURL: https://gitlab.com/spt3g/kubernetes.git
      path: "apps/decentci/sidecars/keycloak-sidecar"
      targetRevision: "main"
    values:
      keycloak:
        ingress:
          hostname: "keycloak.spt3g.ncsa.illinois.edu"
          extraTls:
          - hosts:
            - "keycloak.spt3g.ncsa.illinois.edu"
            secretName: "keycloak-spt3g-ncsa-illinois-edu-tls"
  wordpress:
    enabled: true
    project: spt3g
    sidecar:
      enabled: true
      project: spt3g
      repoURL: https://gitlab.com/spt3g/kubernetes.git
      path: "apps/decentci/sidecars/wordpress-sidecar"
      targetRevision: "main"
    values:
      wordpress:
        existingSecret: "wordpress-admin"
        wordpressBlogName: "SPT-3G"
        smtpHost: "outbound-relays.techservices.illinois.edu"
        smtpPort: "25"
        smtpFromEmail: no-reply@illinois.edu
        replicaCount: 2
        ingress:
          enabled: true
          hostname: "spt3g.ncsa.illinois.edu"
          extraTls:
          - hosts:
            - "spt3g.ncsa.illinois.edu"
            secretName: "spt3g-ncsa-illinois-edu-tls"
        mariadb:
          auth:
            existingSecret: "wordpress-db"
  discourse:
    enabled: false
  nextcloud:
    enabled: false
    project: spt3g
    sidecar:
      enabled: false
      project: spt3g
      repoURL: https://gitlab.com/spt3g/kubernetes.git
      path: "apps/decentci/sidecars/nextcloud-sidecar"
      targetRevision: "main"
    values:
      nextcloud:
        #########################################################################
        ## When upgrading Nextcloud, use delayed startup probe and single replica
        ##
        # startupProbe:
        #   enabled: true
        #   initialDelaySeconds: 600
        #   periodSeconds: 60
        #   timeoutSeconds: 60
        #   failureThreshold: 30
        #   successThreshold: 1
        # replicaCount: 1
        ##
        #########################################################################
        replicaCount: 2
        # image:
        #   tag: 24.0.3-apache
        persistence:
          storageClass: "nfs-taiga"
        ingress:
          tls:
          - secretName: "cloud-spt3g-ncsa-illinois-edu-tls"
            hosts:
            - "cloud.spt3g.ncsa.illinois.edu"
        nextcloud:
          host: cloud.spt3g.ncsa.illinois.edu
          configs:
            extra.config.php: |-
              <?php
              $CONFIG = array (
                'trusted_proxies' => array('traefik'),
                'mail_smtphost' => 'outbound-relays.techservices.illinois.edu',
                'mail_from_address' => 'no-reply',
                'mail_domain' => 'illinois.edu',
                'overwriteprotocol' => 'https',
                'overwrite.cli.url' => 'https://cloud.spt3g.ncsa.illinois.edu'
              );
  jupyterhub:
    enabled: false
  uptimekuma:
    enabled: true
    project: spt3g
    sidecar:
      enabled: true
      project: spt3g
      repoURL: https://gitlab.com/spt3g/kubernetes.git
      path: "apps/decentci/sidecars/uptime-kuma-sidecar"
      targetRevision: "main"
    values:
      ingress:
        hostname: "status.musesframework.io"
