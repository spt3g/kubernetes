This ArgoCD app [derives from the original production app](https://gitlab.com/ncsa-blast/kubernetes/-/tree/main/apps/blast) to deploy Blast on an independent cluster.
