# SPT-3G Documentation

This is the source code for the Sphinx-based documentation site at https://spt3g.ncsa.illinois.edu/docs/.

## Build and push the Docker image for deployment

After building and pushing the Docker image using the commands below, the deployment should automatically update within ten minutes via Keel.

```bash
docker build . -t registry.gitlab.com/spt3g/kubernetes/docs:latest
docker push registry.gitlab.com/spt3g/kubernetes/docs:latest
```

