User Manual
===============================

Cutout Service
--------------------------------


### Introduction
------------

The cutout service allows users to launch asynchronous jobs to generate thumbnails from SPT-3G maps centered on user-supplied RA/DEC coordinates and side dimensions.

### Cutter Web App
------------------

Before proceeding make sure you have registered as a user of the service. Please follow [these instructions](https://spt3g.ncsa.illinois.edu/docs/user/registration.html)

The SPT-3G cutout server is located at: [https://spt3g.ncsa.illinois.edu/app/cutout](https://spt3g.ncsa.illinois.edu/app/cutout)

![](/images/web.png)


### File Types for cutouts
--------------------------

The cutout server has the ability to generate cutouts from SPT3G maps of two different flavors:
- **Passthrough** (extension `psth`), cuts from the _passthrough_ fits maps (i.e. original `g3` files were not touched, just dumped as fits)
- **Filtered** (extension `fltd`), cuts from the _filtered_ fits files. Here filtering means the SPT3G transient filtering, but _without_ the subtraction of the coadds.

    ![](/images/Filetypes.png)


### Map Types for cutouts
-------------------------

The cutout server also has the ability to cut from two types of SPT3G maps:
- **Single Observation Maps**, these are the maps from individual observation scans from 3G. We try to stay up-to-date with 3G files at NCSA. At the moment the server can generate cutouts for Single Observations maps with the date range  **2019-01-01** to **2021-11-30**. The server has the ability to make cutouts from Single Observation maps that can be both `passthrough` and `filtered`. You can make this selection in the "Single Observation Section" of the web interphase.

    ![](/images/dates.png)

- **Yearly Coadds**, these are the yearly coadd maps. These are _curated_ maps and at the moment we have 3 versions to choose from:
    - **winter 2020**, these are the canonical yearly coadds from the spt3g pipeline.
    - **rawmap_v1_2019_2020**, custom rawmap coadds from Melanie Archipley.
    - **cleanbeammap_v1_2019_2020**, custom filtered coadds from Melanie Archipley.

    **Note**: Only for the winter2020 both `passthrough` and `filtered` filetypes are available. For other coadds only `passthrough`.

    ![](/images/coadd_selection.png)


### List of objects and Coordinates
-----------------------------------
The server accepts a comma-separated values (csv) list with columns of two types, with or without an identifier column (`OBJID`) for the coordinates. The `RA,DEC` coordinates need to be in sexagesimal degrees and `XSIZE, YSIZE` need to be in arcminutes. Note that `XSIZE, YSIZE` can be different (or the same) for each coordinate pair. The optional `OBJID` needs to have different entries for each row.

- Without `OBJID`, columns are: `RA,DEC,XSIZE,YSIZE`

    An example list:
    ```
    RA,DEC,XSIZE,YSIZE
    358.3406816, -58.9660379, 10, 10
    355.8253677, -57.1623017, 10, 10
    332.6717176, -50.4503607, 10, 10
    339.6060170, -51.2397203, 10, 10
    334.9264663, -52.9082500, 10, 10
    ```

- With `OBJID`, columns are: `OBJID,RA,DEC,XSIZE,YSIZE`

    An example list with `OBJID`:
    ```
    OBJID,RA,DEC,XSIZE,YSIZE
    OBJ_01, 358.3406816, -58.9660379, 10, 10
    OBJ_02, 355.8253677, -57.1623017, 10, 10
    OBJ_03, 332.6717176, -50.4503607, 10, 10
    OBJ_04, 339.6060170, -51.2397203, 10, 10
    OBJ_05, 334.9264663, -52.9082500, 10, 10
    OBJ_06, 20.9509332,  -46.3929102, 10, 10
    ```


    ![](/images/coordinates.png)

### Lightcurves
---------------

The server provides the option to retrieve lightcurves for each pair of coordinates (i.e. object) supplied. The data points for each lightcurve represent the pixel values at the requested coordinate of each object for which a cutout was generated. The "flux" comes from the first HDU of the fits file, and the associated uncertainty from the second HDU. The lightcurves are written as Python dictionaries in `yaml` format and contained in the file `lightcurve.yaml`.

### Generate cutouts
--------------------

Press, only once the `GENERATE CUTOUTS` button to submit your job.

### Job Status
--------------

The status of all the jobs submitted by the users is available on the `STATUS` page.

![](/images/status-job.png)

### Email notification and Retrieving Files
--------------------------------------------

![](/images/email-job.png)

If you selected `Send email notification` (recommened), after your job has been completed you will receive an email notification, with instructions on how to retrieve your files. For small jobs, you can download/inspect indivual files using a web browser. For larger jobs we recommend bulk downloads using `wget`.

### Back-end Code
-----------------

The back-end code for the web app is the open source and python-based package [`spt3g_cutter`](https://github.com/CMB-NCSA/spt3g_cutter).

### SPT-3G Cutter API
---------------------

For users who do not want to use the web app, we recommend the SPT-3G API.

ℹ️ [Click here to explore the SPT-3G API documentation](../api).

# API Tutorial

**Disclaimer**: This example script may not be maintained diligently, although the basics should be stable. [Check the docs](https://spt3g.ncsa.illinois.edu/docs/) for the latest version of the API example script.

📄 [Copy this entire folder to your home folder](https://gitlab.com/spt3g/kubernetes/-/tree/main/scripts/cutout_service/API_tutorial).

Generate an API token by visiting [your SPT-3G account page](https://spt3g.ncsa.illinois.edu/account/token/create). You can also find an API token displayed on the Home page of the [Cutout web app](https://spt3g.ncsa.illinois.edu/app).

```bash

export SPT_API_TOKEN="PASTE_TOKEN_HERE"
```

Update the file `job_config.yaml` as needed and run.

```bash
cd ~/API_tutorial
./API_example.py
```

### Need Help
-------------
Feel free to send comments or suggestions for upgrades and improvements to Felipe Menanteau and/or Andrew Manning
