User Account Registration
=========================

SPT-3G user accounts are created automatically by our [authentication system](/authentication) upon first login, based on the identity information received from the identity provider selected by the user for their authentication. In order to access SPT-3G services, however, users need to be _authorized_ by the SPT-3G admins.

The registration process for new users is mostly automated. The process is as follows:

1. A new user visits `https://spt3g.ncsa.illinois.edu/account` (or any page requiring authentication) and authenticates with their preferred identity provider. Upon successful authentication they should see the following page:

    ![](/images/upload_6a3837b73975ac1e14f3b72067dd7f30.png)

1. The admins then receive a registration approval email:

    ![](/images/upload_8ca0d674cb081882ddf56d3204d624b5.png)

1. An admin opens the link to approve the request (admin authentication is required):

    ![](/images/upload_0752d69f24fb46fdd35ca7105201bc46.png)

1. The approved user receives a confirmation email providing them convenient links to access services:

    ![](/images/upload_8d3726f3c005770cc9071a032488ac39.png)
