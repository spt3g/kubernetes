SPT-3G Data Services
====================

The `National Center for Supercomputing Applications (NCSA) <https://www.ncsa.illinois.edu>`_ at the University of Illinois develops and operates data services for the South Pole Telescope - 3rd Generation (SPT-3G) project.

This documentation includes resources for end users of the data services as well as information for system administrators and app developers. At NCSA, we are innovating both at the scientific application level -- such as the cutout service -- and at the level of the research platform, where our Kubernetes-based deployment provides reproducibility, scalability, and the *flexibility* modern scientific enterprise demands.

⚠️ Service Status
---------------

If you encounter problems using the SPT-3G services, visit `our real-time service status monitor <https://status.musesframework.io/status/spt3g>`_. If the help form at https://spt3g.ncsa.illinois.edu/app is not working, please 📧 :doc:`email us about the issue <admin/administration>`.

.. toctree::
   :maxdepth: 2
   :caption: User Docs
   :glob:

   user/*

.. toctree::
   :maxdepth: 1
   :caption: Admin and Developer Docs
   :glob:

   admin/*
