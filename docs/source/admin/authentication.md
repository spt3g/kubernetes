Identity and Access Management
===========================================

Keycloak
----------------------------

SPT-3G uses a [Keycloak](https://www.keycloak.org/) server for authentication, which uses [CILogon](http://cilogon.org/) as the identity provider (IdP). The beauty of this system is that we can register a single client application with CILogon for use by Keycloak. Then, we can create as many client applications in Keycloak as we need for our various services. All of our services only depend on Keycloak; they are agnostic to the IdP Keycloak uses. CILogon allows users to choose their own IdP from a very long list of supported IdPs. We have the option to limit this list to some subset we wish to allow.

[Keycloak realm configuration](../_static/20220701-realm-spt3g-export.json)
