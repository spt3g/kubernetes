Bootstrap Kubernetes cluster with Terraform
===========================================

Initial bootstrap of the Kubernetes cluster using [this version of the Terraform script](https://gitlab.com/spt3g/terraform/-/commit/8acbd68a19ac34610c5e56c129fbd91da1f48837) branched from the [ISDA/terraform](https://git.ncsa.illinois.edu/isda/terraform/) repo `master` branch. 1 control-plane node and 2 workers.

`spt3g.tfvars`:

```plain
openstack_projectid   = "e1a...bd3a"

cluster_name          = "spt3g"
cluster_description   = "SPT-3G cluster"
controlplane_count    = 1
worker_count          = 2
worker_flavor         = "m1.xlarge"
metallb_floating_ip   = 1
ingress_controller    = "none"
traefik_dashboard     = false
longhorn_enabled      = false
monitoring_enabled    = false
```

The API key and secret credentials we created in the Radiant dashboard did not provide the permissions needed to create resources when the terraform script ran, so we had to use personal credentials.

The generated `README` file:

```bash
# ----------------------------------------------------------------------
# OPENSTACK SERVERS
# ----------------------------------------------------------------------
openstack private key = $HOME/.ssh/spt3g.pem

# control-plane nodes
ssh -i $HOME/.ssh/spt3g.pem centos@141.142.220.237

# ----------------------------------------------------------------------
# METAL LB (load balancer)
# ----------------------------------------------------------------------
floating ip 0 = 141.142.221.207 == 192.168.1.174

# ----------------------------------------------------------------------
# KUBERNETES
# ----------------------------------------------------------------------
kubeconfig = $HOME/.kube/spt3g.kubeconfig
export KUBECONFIG="$HOME/.kube/spt3g.kubeconfig"
```

The `initialize.sh` script that was generated is mostly inapplicable to our case, because we plan to deploy the infrastructure level applications via ArgoCD. The only parts of the script we retained are:

```bash
#!/bin/bash

# change directory
cd $(dirname $0)

# switch to correct cluster config
export KUBECONFIG="$HOME/.kube/spt3g.kubeconfig"

# make sure cluster folder exists in storage condo
ssh -o StrictHostKeyChecking=no -i spt3g.pem centos@141.142.220.237 \
  'sudo mount radiant-nfs.ncsa.illinois.edu:/radiant/projects/bbfl /mnt && ' \
  'sudo mkdir -p /mnt/spt3g/backup && ' \
  'sudo umount /mnt'

# patch weave to allow for externalPolicy: Local
if [ "$(kubectl get daemonsets.apps -n kube-system weave-net -o yaml | grep NO_MASQ_LOCAL)" == "" ]; then
  echo '# Patching weavenet to allow externalPolicy=Local'
  kubectl patch daemonsets.apps -n kube-system weave-net --type='json' -p='[{"op": "add", "path": "/spec/template/spec/containers/0/env/-", "value": {"name": "NO_MASQ_LOCAL", "value": "1" } }]' --dry-run=none
fi

```

Run the script:

```bash
$ bash spt3g_initialize.sh 
Warning: Permanently added '141.142.220.237' (ECDSA) to the list of known hosts.
# Patching weavenet to allow externalPolicy=Local
daemonset.apps/weave-net patched
```
