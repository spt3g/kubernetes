Administration and Access
=============================

[📧 Email the admins with questions or concerns using this link](mailto:felipe@illinois.edu,manninga@illinois.edu?subject=[SPT-3G]%20Question/Comment).

The administrators of the Kubernetes cluster are [Felipe Menanteau](https://www.ncsa.illinois.edu/people/staff-directory/?contact=felipe) and [Andrew Manning](https://www.ncsa.illinois.edu/people/staff-directory/?contact=manninga). 

Access to deployments and secrets originates from

- access to the Kubernetes cluster controlplane nodes host OS via SSH, and
- membership in the SPT3G (`spt3g`) GitLab group.

The kubeconfig that provides admin access to Kubernetes resources is provided by the [NCSA Rancher instance](https://gonzo-rancher.ncsa.illinois.edu) that is configured to manage the cluster. This provides `kubectl` access to all cluster resources.

GitLab authentication is used to provide access to [the ArgoCD instance](https://argocd.spt3g.ncsa.illinois.edu) and the deployment repo that together drive the deployment of services.
